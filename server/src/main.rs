mod cli;
mod game;
mod glue;
mod myactix;

const SERVER_NAME: &str = "WASM-ACTIX-SERVER";
static MAX_NUMBER_PLAYERS: u32 = 256;
static MAX_NUMBER_SESSIONS: u32 = 32;

#[derive(Clone)]
pub struct Arguments {
    hostname: String,
    port: u16,
    static_dir: std::path::PathBuf,
}

#[tokio::main]
async fn main_tokio(args: Arguments) -> std::io::Result<()> {
    let local = tokio::task::LocalSet::new();
    let (pool_front, _pool) = glue::create_pool(
        SERVER_NAME.to_string(),
        MAX_NUMBER_PLAYERS,
        MAX_NUMBER_SESSIONS,
    );
    myactix::main_tokio(&local, args, pool_front).await
}

fn main() {
    main_tokio(cli::run("hello-wasm-actix-server")).unwrap();
}
