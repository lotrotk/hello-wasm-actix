use clap::{App, Arg};

static ARG_HOSTNAME: &str = "hostname";
static ARG_PORT: &str = "port";
static ARG_STATIC: &str = "static";

pub fn run(name: &str) -> super::Arguments {
    let matches = App::new(name)
        .version("1.0")
        .author("Arne J. <lotrotk@gmail.com>")
        .about("Runs wasm pages")
        .arg(
            Arg::with_name(ARG_HOSTNAME)
                .short("h")
                .long("hostname")
                .value_name("HOSTNAME")
                .default_value("localhost")
                .takes_value(true),
        )
        .arg(
            Arg::with_name(ARG_PORT)
                .short("p")
                .long("port")
                .value_name("PORT")
                .default_value("8000")
                .takes_value(true),
        )
        .arg(
            Arg::with_name(ARG_STATIC)
                .short("s")
                .long("static-dir")
                .value_name("DIR")
                .help("The contents of a directory containing ld.js and ld_bg.wasm")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    let hostname = matches.value_of(ARG_HOSTNAME).unwrap().to_string();
    let port = matches
        .value_of(ARG_PORT)
        .unwrap()
        .parse::<u16>()
        .expect("invalid port");
    let static_dir: std::path::PathBuf = matches.value_of(ARG_STATIC).unwrap().into();
    super::Arguments {
        hostname,
        port,
        static_dir,
    }
}
