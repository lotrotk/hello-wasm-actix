use super::*;

mod mines;

impl From<game::GameID> for lib::Session {
    fn from(game: game::GameID) -> Self {
        let game::GameID(number) = game;
        Self(number)
    }
}

impl From<lib::Session> for game::GameID {
    fn from(session: lib::Session) -> Self {
        let lib::Session(number) = session;
        Self(number)
    }
}

impl From<game::GameType> for lib::GameType {
    fn from(game_type: game::GameType) -> Self {
        let game::GameType::Mines = game_type;
        Self::Mines
    }
}

impl From<game::Score> for lib::Score {
    fn from(score: game::Score) -> Self {
        let game::Score(number) = score;
        Self(number)
    }
}

/*
 *from_client
 */

impl From<lib::from_client::GameMessage> for game::from_client::GameMessage {
    fn from(msg: lib::from_client::GameMessage) -> Self {
        let lib::from_client::GameMessage::Mines(mines) = msg;
        Self::Mines(mines.into())
    }
}

impl From<(game::PlayerID, lib::from_client::Message)> for game::from_client::Message {
    fn from((player, msg): (game::PlayerID, lib::from_client::Message)) -> Self {
        let content = match msg {
            lib::from_client::Message::PlayerRegistration { name } => {
                game::from_client::Content::PlayerRegistration { name }
            }
            lib::from_client::Message::QuitGame(session) => {
                let game_id = session.into();
                game::from_client::Content::QuitGame(game_id)
            }
            lib::from_client::Message::GameMessage(msg) => {
                game::from_client::Content::GameMessage(msg.into())
            }
            lib::from_client::Message::Participate { session, title } => {
                game::from_client::Content::Participate {
                    session: session.into(),
                    title,
                }
            }
        };
        Self(player, content)
    }
}

/*
 *from_server
 */

impl From<game::from_server::PlayerRegistrationReply>
    for lib::from_server::PlayerRegistrationReply
{
    fn from(reply: game::from_server::PlayerRegistrationReply) -> Self {
        match reply {
            game::from_server::PlayerRegistrationReply::Failure(reason) => Self::Failure(reason),
            game::from_server::PlayerRegistrationReply::RegisteredWithoutName => {
                Self::RegisteredWithoutName
            }
            game::from_server::PlayerRegistrationReply::RegisteredWithName(name) => {
                Self::RegisteredWithName(name)
            }
            game::from_server::PlayerRegistrationReply::RegisteredWithNameFailed {
                name,
                reason,
            } => Self::RegisteredWithNameFailed { name, reason },
        }
    }
}

impl From<game::from_server::SessionDescription> for lib::from_server::SessionDescription {
    fn from(descr: game::from_server::SessionDescription) -> Self {
        let game::from_server::SessionDescription { game_type, title } = descr;
        Self {
            game_type: game_type.into(),
            title,
        }
    }
}

impl From<game::from_server::ServerStatus> for lib::from_server::ServerStatus {
    fn from(status: game::from_server::ServerStatus) -> Self {
        let game::from_server::ServerStatus { name, sessions } = status;
        let sessions = sessions
            .into_iter()
            .map(|(game_id, descr)| (game_id.into(), descr.into()))
            .collect();
        Self { name, sessions }
    }
}

impl From<game::from_server::Message> for lib::from_server::Message {
    fn from(msg: game::from_server::Message) -> Self {
        match msg {
            game::from_server::Message::Participation(game::from_server::Participation {
                session,
                player,
                player_is_you,
            }) => Self::Participation(lib::from_server::Participation {
                session: session.into(),
                player,
                player_is_you,
            }),
            game::from_server::Message::PlayerRegistrationReply(reply) => {
                Self::PlayerRegistrationReply(reply.into())
            }
            game::from_server::Message::Status(status) => Self::Status(status.into()),
            game::from_server::Message::Mines { session, game } => Self::Mines {
                session: session.into(),
                game: game.into(),
            },
            game::from_server::Message::FailedToMakeGame { title, reason } => {
                Self::FailedToMakeGame { title, reason }
            }
            game::from_server::Message::QuitGame {
                player,
                session,
                score,
            } => Self::QuitGame {
                player,
                session: session.into(),
                score: score.into(),
            },
        }
    }
}
