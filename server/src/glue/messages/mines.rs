use super::*;

use game::messages::mines as game;
use lib::mines as lib;

impl From<lib::Coordinate> for game::Coordinate {
    fn from(c: lib::Coordinate) -> Self {
        let lib::Coordinate { x, y } = c;
        Self { y, x }
    }
}

impl From<game::Coordinate> for lib::Coordinate {
    fn from(c: game::Coordinate) -> Self {
        let game::Coordinate { x, y } = c;
        Self { y, x }
    }
}

impl From<lib::NewGame> for game::NewGame {
    fn from(new_game: lib::NewGame) -> Self {
        let lib::NewGame {
            width,
            height,
            mines,
            title,
        } = new_game;
        Self {
            width,
            height,
            mines,
            title,
        }
    }
}

impl From<game::NewGame> for lib::NewGame {
    fn from(new_game: game::NewGame) -> Self {
        let game::NewGame {
            width,
            height,
            mines,
            title,
        } = new_game;
        Self {
            width,
            height,
            mines,
            title,
        }
    }
}

/*
 *from_client
 */

impl From<lib::from_client::CellMarking> for game::from_client::CellMarking {
    fn from(marking: lib::from_client::CellMarking) -> Self {
        match marking {
            lib::from_client::CellMarking::Clear => Self::Clear,
            lib::from_client::CellMarking::Mine => Self::Mine,
            lib::from_client::CellMarking::Question => Self::Question,
            lib::from_client::CellMarking::Safe => Self::Safe,
        }
    }
}

impl From<lib::from_client::Message> for game::from_client::Message {
    fn from(msg: lib::from_client::Message) -> Self {
        match msg {
            lib::from_client::Message::Marking(session, coordinate, marking) => {
                Self::Marking(session.into(), coordinate.into(), marking.into())
            }
            lib::from_client::Message::RequestNewGame(newgame) => {
                Self::RequestNewGame(newgame.into())
            }
        }
    }
}

/*
 *from_server
 */

impl From<game::from_server::Tile> for lib::from_server::Tile {
    fn from(tile: game::from_server::Tile) -> Self {
        match tile {
            game::from_server::Tile::Covered => Self::Covered,
            game::from_server::Tile::EmptyWithNeighboringMines(number) => {
                Self::EmptyWithNeighboringMines(number)
            }
            game::from_server::Tile::ExplodedMine => Self::ExplodedMine,
            game::from_server::Tile::MarkingThatIsCorrect => Self::MarkingThatIsCorrect,
            game::from_server::Tile::MarkingThatIsIncorrect => Self::MarkingThatIsIncorrect,
            game::from_server::Tile::MarkingThatMayBeCorrectOrIncorrect => {
                Self::MarkingThatMayBeCorrectOrIncorrect
            }
            game::from_server::Tile::Mine => Self::Mine,
            game::from_server::Tile::Questioned => Self::Questioned,
        }
    }
}

impl From<game::from_server::Marking> for lib::from_server::Marking {
    fn from(marking: game::from_server::Marking) -> Self {
        match marking {
            game::from_server::Marking::UpdatedTiles(tiles) => {
                let tiles = tiles
                    .into_iter()
                    .map(|(coordinate, tile)| (coordinate.into(), tile.into()))
                    .collect();
                Self::UpdatedTiles(tiles)
            }
            game::from_server::Marking::Failure(reason) => Self::Failure(reason),
        }
    }
}

impl From<game::from_server::PlayerStats> for lib::from_server::PlayerStats {
    fn from(stats: game::from_server::PlayerStats) -> Self {
        let game::from_server::PlayerStats {
            player_name,
            mines_marked_correctly,
            mines_marked_incorrectly,
            cleared_correct_markings,
            cleared_incorrect_markings,
            actions,
            questioned,
            cleared_questioned,
        } = stats;
        Self {
            player_name,
            mines_marked_correctly,
            mines_marked_incorrectly,
            cleared_correct_markings,
            cleared_incorrect_markings,
            actions,
            questioned,
            cleared_questioned,
        }
    }
}

impl From<game::from_server::Message> for lib::from_server::Message {
    fn from(msg: game::from_server::Message) -> Self {
        match msg {
            game::from_server::Message::ConfirmNewGame(new_game) => {
                Self::ConfirmNewGame(new_game.into())
            }
            game::from_server::Message::Lost {
                player_that_did_boom,
            } => Self::Lost {
                player_that_did_boom,
            },
            game::from_server::Message::Marking(marking) => Self::Marking(marking.into()),
            game::from_server::Message::PlayerStats(stats) => {
                let stats = stats.into_iter().map(|stat| stat.into()).collect();
                Self::PlayerStats(stats)
            }
            game::from_server::Message::Score { player, score } => {
                let score: hello_wasm_actix_lib::messages::Score = score.into();
                Self::Score { player, score }
            }
            game::from_server::Message::Won => Self::Won,
        }
    }
}
