mod messages;

use hello_wasm_actix_lib::messages as lib;

use std::sync::{Arc, Mutex};

use futures::channel::mpsc as fmpsc;

use super::game;
use super::myactix;

const CHANNEL_FROM_SERVER_TO_CLIENT_BUFFER_SIZE: usize = 32;

type SenderFromServerToClient = fmpsc::Sender<myactix::Message>;
type ReceiverFromServerToClient = fmpsc::Receiver<myactix::Message>;
type PlayerToServer = game::PlayerToServer<SenderFromServerToClient>;
pub struct ServerToClientChannelFactory;

impl myactix::ReceiverFromServerToClient for ReceiverFromServerToClient {}

impl game::SenderFromServerToClient for SenderFromServerToClient {
    fn send(&mut self, msg: game::from_server::Message) {
        let msg: lib::from_server::Message = msg.into();
        let msg = myactix::Message::Text(lib::serialize(&msg));
        self.try_send(msg)
            .expect("Failed to send message from server to client");
    }
}

impl game::ServerToClientChannelFactory for ServerToClientChannelFactory {
    type Sender = SenderFromServerToClient;
    type Receiver = ReceiverFromServerToClient;

    fn channel(&mut self) -> (SenderFromServerToClient, Self::Receiver) {
        futures::channel::mpsc::channel(CHANNEL_FROM_SERVER_TO_CLIENT_BUFFER_SIZE)
    }
}

impl myactix::SenderFromClientToServer for PlayerToServer {
    fn on_stopped(&self) {
        let content = game::from_client::Content::Disconnected;
        let msg = game::from_client::Message(self.id, content);
        self.sender_from_client_to_server.send(msg.into()).ok();
    }

    fn on_message(&self, msg: myactix::Message) {
        let myactix::Message::Text(msg) = msg;
        if let Ok(msg) = lib::deserialize::<lib::from_client::Message>(&msg) {
            let msg: game::from_client::Message = (self.id, msg).into();
            self.sender_from_client_to_server.send(msg.into()).unwrap();
        }
    }
}

impl<PF> myactix::SocketPool<ReceiverFromServerToClient, PlayerToServer> for Arc<Mutex<PF>>
where
    PF: game::PlayerPoolFront<
        Sender = SenderFromServerToClient,
        Receiver = ReceiverFromServerToClient,
    >,
{
    fn on_new_connection(&self) -> Option<(ReceiverFromServerToClient, PlayerToServer)> {
        let pool = &mut self.lock().unwrap();
        pool.create_player()
    }
}

pub fn create_pool(
    server_name: String,
    max_players: u32,
    max_sessions: u32,
) -> (
    Arc<
        Mutex<
            impl game::PlayerPoolFront<
                Sender = SenderFromServerToClient,
                Receiver = ReceiverFromServerToClient,
            >,
        >,
    >,
    impl game::PlayerPoolThread,
) {
    game::create_pool(
        server_name,
        max_players,
        max_sessions,
        ServerToClientChannelFactory {},
    )
}
