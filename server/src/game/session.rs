use super::*;

use std::collections::HashSet;

pub struct Session {
    _id: GameID,
    title: String,
    game: Game,
    participaters: HashSet<PlayerID>,
}

impl Session {
    pub fn new(id: GameID, title: String, game: Game) -> Self {
        let participaters = HashSet::new();
        Self {
            _id: id,
            title,
            game,
            participaters,
        }
    }

    pub fn get_type(&self) -> GameType {
        match &self.game {
            session::Game::Mines(_) => GameType::Mines,
        }
    }

    pub fn get_title(&self) -> &String {
        &self.title
    }

    pub fn mut_game(&mut self) -> &mut Game {
        &mut self.game
    }

    pub fn get_participaters(&'_ self) -> impl Iterator<Item = PlayerID> + '_ {
        self.participaters.iter().cloned()
    }

    pub fn add_participater(&mut self, player: PlayerID) {
        let new_player = self.participaters.insert(player);
        assert!(new_player, "This player has already joined this game");
        self.game
            .as_mut_game_player_access()
            .insert_player(player, GamePlayerAccessKey::new());
    }

    pub fn remove_participater(&mut self, player: PlayerID) -> Score {
        let had_player = self.participaters.remove(&player);
        assert!(had_player, "This player has not joined this game");
        self.game
            .as_mut_game_player_access()
            .remove_player(player, GamePlayerAccessKey::new())
    }
}

pub enum Game {
    Mines(mines::Session),
}

pub struct GamePlayerAccessKey {
    _key: std::marker::PhantomData<()>,
}

impl GamePlayerAccessKey {
    fn new() -> Self {
        Self {
            _key: std::marker::PhantomData,
        }
    }

    #[cfg(test)]
    pub fn new_for_test() -> Self {
        Self::new()
    }
}

pub trait GamePlayerAccess {
    fn insert_player(&mut self, player: PlayerID, key: GamePlayerAccessKey);
    fn remove_player(&mut self, player: PlayerID, key: GamePlayerAccessKey) -> Score;
}

impl Game {
    fn as_mut_game_player_access(&mut self) -> &mut dyn GamePlayerAccess {
        match self {
            Self::Mines(mines_session) => mines_session,
        }
    }
}
