use super::*;

use crate::game::mines;

pub struct NewGame(pub GameID);

impl<S: SenderFromServerToClient> Player<S> {
    pub(super) fn on_client_mines_message<
        PM: definitions::PlayerInfoManager,
        SM: definitions::SessionManager,
    >(
        &mut self,
        players: &PM,
        sessions: &mut SM,
        msg: messages::mines::from_client::Message,
    ) -> Result<Option<NewGame>, &'static str> {
        match msg {
            messages::mines::from_client::Message::RequestNewGame(new_game) => {
                let messages::mines::NewGame {
                    width,
                    height,
                    mines,
                    title,
                } = new_game;
                Ok(self.take_new_mines_game_action(players, sessions, width, height, mines, title))
            }
            messages::mines::from_client::Message::Marking(session, coords, marking) => self
                .take_mines_mark_cell_action(players, sessions, session, coords, marking)
                .map(|()| None),
        }
    }

    fn take_new_mines_game_action<
        PM: definitions::PlayerInfoManager,
        SM: definitions::SessionManager,
    >(
        &mut self,
        players: &PM,
        sessions: &mut SM,
        width: u32,
        height: u32,
        mines: u32,
        title: String,
    ) -> Option<NewGame> {
        let new_session = (|| {
            let new_session =
                mines::Session::try_new(width, height, mines).map(session::Game::Mines)?;
            sessions.add_new_session_and_add_player_to_it(title.clone(), new_session, self.id)
        })();
        let ((reply, optional_reply), new_game) = match new_session {
            Ok(new_session) => {
                let is_new_session = self.participating.insert(new_session);
                assert!(is_new_session, "session already exists");
                let new_game = messages::mines::NewGame {
                    width,
                    height,
                    mines,
                    title,
                };
                let game = messages::mines::from_server::Message::ConfirmNewGame(new_game);
                let reply = from_server::Message::Mines {
                    session: new_session,
                    game,
                };
                let optional_reply = Some(messages::from_server::Message::Participation(
                    messages::from_server::Participation {
                        session: new_session,
                        player: players.name(self.id).into(),
                        player_is_you: true,
                    },
                ));
                ((reply, optional_reply), Some(NewGame(new_session)))
            }
            Err(reason) => {
                eprintln!("Failed to create game 'mines' with width={}, height = {}, mines = {}, reason = {}", width, height, mines, reason);
                let reply = from_server::Message::FailedToMakeGame {
                    title,
                    reason: reason.into(),
                };
                ((reply, None), None)
            }
        };
        self.channel_to_client.send(reply);
        if let Some(reply) = optional_reply {
            self.channel_to_client.send(reply);
        }
        new_game
    }

    fn take_mines_mark_cell_action<
        PM: definitions::PlayerInfoManager,
        SM: definitions::SessionManager,
    >(
        &mut self,
        players: &PM,
        sessions: &mut SM,
        game_id: GameID,
        coords: messages::mines::Coordinate,
        marking: messages::mines::from_client::CellMarking,
    ) -> Result<(), &'static str> {
        let game = &mut sessions
            .find(game_id)
            .ok_or("Unable to find session")?
            .mut_game();
        let session::Game::Mines(mines) = game;

        let index = mines::Index::from_coordinates_yx(mines, coords.y, coords.x);
        let reply = mines.mark(self.id, index, marking.into());
        match reply {
            mines::MarkReply::Invalid(reason) => {
                println!(
                    "Player did invalid mines action, not sending a reply: {}",
                    reason
                );
            }
            mines::MarkReply::Valid { update, finished } => {
                let updates = update
                    .iter()
                    .map(|(index, info)| {
                        let (y, x) = index.to_coordinates_yx(mines);
                        let coords = messages::mines::Coordinate { x, y };
                        let cell = (*info).into();
                        (coords, cell)
                    })
                    .collect();
                let reply = messages::mines::from_server::Marking::UpdatedTiles(updates);
                let game = messages::mines::from_server::Message::Marking(reply);
                let msg = messages::from_server::Message::Mines {
                    session: game_id,
                    game,
                };
                self.channel_to_client.send(msg);
                if let Some(finished) = finished {
                    self.take_action_when_mines_finished(players, game_id, finished);
                }
            }
        }
        Ok(())
    }

    fn take_action_when_mines_finished<PM: definitions::PlayerInfoManager>(
        &mut self,
        players: &PM,
        game: GameID,
        finished: mines::Finished,
    ) {
        let mines::Finished {
            player_that_did_boom,
            stats,
        } = finished;
        if let Some(player_that_did_boom) = player_that_did_boom {
            let player_that_did_boom = players.name(player_that_did_boom).into();
            let msg = messages::mines::from_server::Message::Lost {
                player_that_did_boom,
            };
            let msg = messages::from_server::Message::Mines {
                session: game,
                game: msg,
            };
            self.channel_to_client.send(msg);
        }
        let stats = stats
            .into_iter()
            .map(|(player, stats_per_player)| convert_stats(players, player, stats_per_player))
            .collect();
        let msg = messages::mines::from_server::Message::PlayerStats(stats);
        let msg = messages::from_server::Message::Mines {
            session: game,
            game: msg,
        };
        self.channel_to_client.send(msg);
    }
}

impl From<messages::mines::from_client::CellMarking> for mines::Covery {
    fn from(marking: messages::mines::from_client::CellMarking) -> Self {
        match marking {
            messages::mines::from_client::CellMarking::Clear => Self::Covered,
            messages::mines::from_client::CellMarking::Mine => Self::Marked,
            messages::mines::from_client::CellMarking::Question => Self::Questioned,
            messages::mines::from_client::CellMarking::Safe => Self::Uncovered,
        }
    }
}

impl From<mines::CellInfo> for messages::mines::from_server::Tile {
    fn from(info: mines::CellInfo) -> Self {
        type Info = mines::CellInfo;
        match info {
            Info::Covered => Self::Covered,
            Info::MarkedCorrectly => Self::MarkingThatIsCorrect,
            Info::MarkedEitherCorrectlyOrIncorrectly => Self::MarkingThatMayBeCorrectOrIncorrect,
            Info::MarkedIncorrectly => Self::MarkingThatIsIncorrect,
            Info::Mine => Self::Mine,
            Info::MineThatDidBoom => Self::ExplodedMine,
            Info::NeighboringMines(number) => Self::EmptyWithNeighboringMines(number),
            Info::Questioned => Self::Questioned,
        }
    }
}

fn convert_stats<PM: definitions::PlayerInfoManager>(
    players: &PM,
    player: PlayerID,
    stats: mines::Statistics,
) -> messages::mines::from_server::PlayerStats {
    let player_name = players.name(player).into();
    let mines::Statistics {
        mines_marked_correctly,
        mines_marked_incorrectly,
        cleared_correct_markings,
        cleared_incorrect_markings,
        actions,
        questioned,
        cleared_questioned,
    } = stats;
    messages::mines::from_server::PlayerStats {
        player_name,
        mines_marked_correctly,
        mines_marked_incorrectly,
        cleared_correct_markings,
        cleared_incorrect_markings,
        actions,
        questioned,
        cleared_questioned,
    }
}

#[cfg(test)]
pub mod tests {
    use super::super::super::definitions::tests::*;
    use super::super::tests::*;
    use super::*;
    use definitions::Player;

    use mockall::{predicate::*, Sequence};

    fn make_new_game() -> messages::mines::NewGame {
        messages::mines::NewGame {
            width: 25,
            height: 25,
            mines: 100,
            title: DEFAULT_SESSION_TITLE.into(),
        }
    }

    fn make_new_game_request_msg() -> from_client::GameMessage {
        let request = messages::mines::from_client::Message::RequestNewGame(make_new_game());
        from_client::GameMessage::Mines(request)
    }

    fn make_new_game_confirmed_msg(session: GameID) -> from_server::Message {
        let msg = messages::mines::from_server::Message::ConfirmNewGame(make_new_game());
        from_server::Message::Mines { session, game: msg }
    }

    fn make_participation_msg(session: GameID, player: String) -> from_server::Message {
        let p = messages::from_server::Participation {
            session,
            player,
            player_is_you: true,
        };
        messages::from_server::Message::Participation(p)
    }

    fn make_player_quit_game_msg(name: String, score: Score) -> from_server::Message {
        from_server::Message::QuitGame {
            player: name,
            session: DEFAULT_SESSION_ID,
            score,
        }
    }

    fn take_action_new_game(
        new_game_result: Result<GameID, &'static str>,
        num_server_replies: usize,
        server_reply: impl Fn(&from_server::Message) -> bool + Send + 'static,
    ) {
        let mut seq = Sequence::new();

        let mut session_manager = MockMySessionManager::new();
        session_manager
            .mock
            .expect_add_new_session_and_add_player_to_it()
            .once()
            .with(
                eq(DEFAULT_SESSION_TITLE.to_string()),
                always(),
                eq(DEFAULT_PLAYER_ID),
            )
            .return_const(new_game_result)
            .in_sequence(&mut seq);

        let ref_seq = &mut seq;
        let mut player = construct_as_player_and(DEFAULT_PLAYER_ID, move |channel_to_client| {
            channel_to_client
                .expect_send()
                .times(num_server_replies)
                .withf(server_reply)
                .return_const(())
                .in_sequence(ref_seq);
        });

        let mut players_info = crate::game::definitions::tests::MockMyPlayerInfoManager::new();
        players_info
            .expect_name()
            .return_const(DEFAULT_PLAYER_NAME.into());

        let msg = make_new_game_request_msg();

        let _update = player
            .take_action_on_message(&mut players_info, &mut session_manager, msg)
            .unwrap();
    }

    #[test]
    fn on_take_new_game_action_will_reply_with_game_confirmed_and_participating_if_a_new_game_is_succesfully_made(
    ) {
        let new_game_result = Ok(DEFAULT_SESSION_ID);
        let server_replies: [from_server::Message; 2] = [
            make_new_game_confirmed_msg(DEFAULT_SESSION_ID),
            make_participation_msg(DEFAULT_SESSION_ID, DEFAULT_PLAYER_NAME.into()),
        ];
        let index = Arc::new(atomic::AtomicUsize::new(0));
        take_action_new_game(new_game_result, 2, move |r| {
            let i = index.fetch_add(1, atomic::Ordering::SeqCst);
            *r == server_replies[i]
        })
    }

    #[test]
    fn on_take_new_game_action_will_reply_with_game_failed_to_make_if_a_new_game_is_not_made() {
        let new_game_result = Err("In this test the game can not be started");
        take_action_new_game(new_game_result, 1, |r| match r {
            from_server::Message::FailedToMakeGame { title, .. } => title == DEFAULT_SESSION_TITLE,
            _ => false,
        });
    }

    #[test]
    fn on_disconnected_when_player_is_already_in_session() {
        let make_player = |id: PlayerID, is_bob_leaving: bool| {
            let mut player = construct_as_player_and(id, |channel_to_client| {
                if !is_bob_leaving {
                    channel_to_client
                        .expect_send()
                        .once()
                        .with(eq(make_player_quit_game_msg("Bob".into(), Score(0))))
                        .return_const(());
                }
            });
            player.participating.insert(DEFAULT_SESSION_ID);
            player
        };

        let alice_id = PlayerID(4);
        let bob_id = PlayerID(8);
        let alice = make_player(alice_id, false);
        let bob = make_player(bob_id, true);

        let mut players = MockMyPlayerManager::new();
        players
            .expect_get_mut()
            .times(..)
            .with(eq(alice_id))
            .return_var(alice);
        players
            .expect_get_mut()
            .times(..)
            .with(eq(bob_id))
            .return_var(bob);

        let mut players_info = crate::game::definitions::tests::MockMyPlayerInfoManager::new();
        players_info
            .expect_name()
            .times(..)
            .with(eq(alice_id))
            .return_const("Alice".into());
        players_info
            .expect_name()
            .times(..)
            .with(eq(bob_id))
            .return_const("Bob".into());

        let mut session_manager = MockMySessionManager::new();
        let session = {
            let mines = crate::game::mines::Session::try_new(20, 20, 25).unwrap();
            let mut session = session::Session::new(
                DEFAULT_SESSION_ID,
                "a session with two participaters".into(),
                session::Game::Mines(mines),
            );
            session.add_participater(alice_id);
            session.add_participater(bob_id);
            session
        };
        session_manager.session = Some(session);
        session_manager
            .mock
            .expect_find()
            .times(..)
            .with(eq(DEFAULT_SESSION_ID))
            .return_const(());

        //start test
        let players_need_update = PlayerMocked::on_disconnected(
            bob_id,
            &mut players,
            &mut session_manager,
            &mut players_info,
        );
        if let definitions::PlayersNeedStatusUpdate::Yes = players_need_update {
            panic!("There is still a participating player in session, no updates needed")
        }

        //check that the session's participaters have not been changed
        let session = session_manager.session.unwrap();
        compare_participaters(session.get_participaters(), std::iter::once(alice_id));
        //check that the participaters in the players is correct
        use crate::game::definitions::PlayerManager;
        compare_participaters(
            players.get_mut(alice_id).participating.iter().cloned(),
            std::iter::once(DEFAULT_SESSION_ID),
        );
        compare_participaters(
            players.get_mut(bob_id).participating.iter().cloned(),
            std::iter::empty(),
        );
    }

    fn compare_participaters<
        T: std::fmt::Debug + Eq,
        X: Iterator<Item = T>,
        Y: Iterator<Item = T> + Clone,
    >(
        x: X,
        y: Y,
    ) {
        let mut used_y = std::collections::HashSet::<usize>::new();
        let mut x_size = 0;

        for idx in x {
            x_size += 1;
            let found_equal = y
                .clone()
                .enumerate()
                .any(|(index, value)| value == idx && used_y.insert(index));
            assert!(found_equal, "The second doesn't contain any more  of that");
        }
        assert_eq!(y.count(), x_size, "More elements in the second")
    }
}
