use super::*;

mod mines;

use std::collections::HashSet;

#[derive(Debug)]
pub struct Player<S: SenderFromServerToClient> {
    id: PlayerID,
    channel_to_client: S,
    participating: HashSet<GameID>,
}

impl<S: SenderFromServerToClient> Player<S> {
    pub fn new(id: PlayerID, channel_to_client: S) -> Self {
        Self {
            id,
            channel_to_client,
            participating: HashSet::new(),
        }
    }

    fn on_quit_every_game<
        P: definitions::PlayerManager<Item = Self>,
        SM: definitions::SessionManager,
        PM: definitions::PlayerInfoManager,
    >(
        player: PlayerID,
        players: &mut P,
        sessions: &mut SM,
        players_info: &PM,
    ) -> definitions::PlayersNeedStatusUpdate {
        let mut participating = HashSet::new();
        std::mem::swap(
            &mut participating,
            &mut players.get_mut(player).participating,
        );
        let mut should_update = definitions::PlayersNeedStatusUpdate::No;
        for session in &participating {
            let session_is_gone = Self::player_notifies_no_longer_participating(
                player,
                *session,
                players,
                sessions,
                players_info,
            );
            if let definitions::PlayersNeedStatusUpdate::Yes = session_is_gone {
                should_update = definitions::PlayersNeedStatusUpdate::Yes;
                sessions.remove(*session);
            }
        }
        should_update
    }

    fn player_notifies_no_longer_participating<
        P: definitions::PlayerManager<Item = Self>,
        SM: definitions::SessionManager,
        PM: definitions::PlayerInfoManager,
    >(
        player: PlayerID,
        session: GameID,
        players: &mut P,
        sessions: &mut SM,
        players_info: &PM,
    ) -> definitions::PlayersNeedStatusUpdate {
        let name = players_info.name(player);
        let session_id = session;
        let session = sessions.find(session_id).unwrap();
        let score = session.remove_participater(player);
        let mut should_update = definitions::PlayersNeedStatusUpdate::Yes;
        for participater in session.get_participaters() {
            let msg = from_server::Message::QuitGame {
                player: name.into(),
                session: session_id,
                score,
            };
            let participater = &mut players.get_mut(participater);
            participater.channel_to_client.send(msg);
            should_update = definitions::PlayersNeedStatusUpdate::No;
        }
        should_update
    }

    fn send_participation_msg_to_participaters<
        P: definitions::PlayerManager<Item = Self>,
        SM: definitions::SessionManager,
        PM: definitions::PlayerInfoManager,
    >(
        player: PlayerID,
        session: GameID,
        players: &mut P,
        sessions: &mut SM,
        players_info: &PM,
    ) {
        let player_id = player;
        let session_id = session;
        let session = sessions.find(session_id).unwrap();
        let msg = messages::from_server::Participation {
            session: session_id,
            player: players_info.name(player_id).into(),
            player_is_you: false,
        };
        for participater_id in session.get_participaters() {
            let participater = players.get_mut(participater_id);
            let mut msg = msg.clone();
            msg.player_is_you = player == participater_id;
            let msg = messages::from_server::Message::Participation(msg);
            participater.channel_to_client.send(msg);
        }
    }
}

impl<S: SenderFromServerToClient + 'static> definitions::Player for Player<S> {
    type SenderFromServerToClient = S;

    fn new(id: PlayerID, channel_to_client: S) -> Self {
        Self::new(id, channel_to_client)
    }

    fn on_quit_game<
        P: definitions::PlayerManager<Item = Self>,
        SM: definitions::SessionManager,
        PM: definitions::PlayerInfoManager,
    >(
        player: PlayerID,
        players: &mut P,
        sessions: &mut SM,
        players_info: &PM,
        session: GameID,
    ) -> definitions::PlayersNeedStatusUpdate {
        let was_participating = players.get_mut(player).participating.remove(&session);
        if !was_participating {
            return definitions::PlayersNeedStatusUpdate::No;
        }
        Self::player_notifies_no_longer_participating(
            player,
            session,
            players,
            sessions,
            players_info,
        )
    }

    fn on_disconnected<
        P: definitions::PlayerManager<Item = Self>,
        SM: definitions::SessionManager,
        PM: definitions::PlayerInfoManager,
    >(
        player: PlayerID,
        players: &mut P,
        sessions: &mut SM,
        players_info: &PM,
    ) -> definitions::PlayersNeedStatusUpdate {
        Self::on_quit_every_game(player, players, sessions, players_info)
    }

    fn take_action_on_message<
        PM: definitions::PlayerInfoManager,
        SM: definitions::SessionManager,
    >(
        &mut self,
        players: &mut PM,
        sessions: &mut SM,
        message: from_client::GameMessage,
    ) -> Result<definitions::PlayersNeedStatusUpdate, &'static str> {
        let from_client::GameMessage::Mines(game) = message;
        self.on_client_mines_message(players, sessions, game)
            .map(|new_game| {
                if let Some(self::mines::NewGame(_)) = new_game {
                    definitions::PlayersNeedStatusUpdate::Yes
                } else {
                    definitions::PlayersNeedStatusUpdate::No
                }
            })
    }

    fn rename(&mut self, name: definitions::RenamePlayer) {
        let reply = match name {
            definitions::RenamePlayer::ValidName(name) => {
                from_server::PlayerRegistrationReply::RegisteredWithName(name)
            }
            definitions::RenamePlayer::InvalidName { name, reason } => {
                from_server::PlayerRegistrationReply::RegisteredWithNameFailed { name, reason }
            }
        };
        self.channel_to_client
            .send(from_server::Message::PlayerRegistrationReply(reply));
    }

    fn participate<P, SM, PM>(
        player: PlayerID,
        session: GameID,
        players: &mut P,
        sessions: &mut SM,
        players_info: &PM,
        title: &str,
    ) where
        P: definitions::PlayerManager<Item = Self>,
        SM: definitions::SessionManager,
        PM: definitions::PlayerInfoManager,
    {
        let player_id = player;
        let session_id = session;
        let session = match sessions.find(session_id) {
            Some(session) => session,
            None => return,
        };
        if session.get_title() != title {
            return;
        }
        if session.get_participaters().any(|id| id == player_id) {
            return;
        }
        session.add_participater(player_id);
        let player = players.get_mut(player_id);
        assert!(
            player.participating.insert(session_id),
            "Is not a new player"
        );
        Self::send_participation_msg_to_participaters(
            player_id,
            session_id,
            players,
            sessions,
            players_info,
        )
    }

    fn send_msg(&mut self, msg: messages::from_server::Message) {
        self.channel_to_client.send(msg);
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use crate::game::definitions::tests::*;
    use crate::game::definitions::Player;
    use crate::game::tests::*;

    use mockall::predicate::*;

    pub const DEFAULT_PLAYER_ID: PlayerID = PlayerID(1);
    pub static DEFAULT_PLAYER_NAME: &str = "Celeste";
    pub const DEFAULT_SESSION_ID: GameID = GameID(1);
    pub const DEFAULT_SESSION_TITLE: &str = "this is my super game";

    pub type PlayerMocked = super::Player<MockMySenderFromServerToClient>;

    #[test]
    fn can_construct() {
        let channel_to_client = MockMySenderFromServerToClient::new();
        let id = DEFAULT_PLAYER_ID;
        let _player = PlayerMocked::new(id, channel_to_client);
    }

    pub fn construct_as_player_and(
        id: PlayerID,
        prepare: impl FnOnce(&mut MockMySenderFromServerToClient),
    ) -> PlayerMocked {
        let mut channel_to_client = MockMySenderFromServerToClient::new();
        prepare(&mut channel_to_client);
        PlayerMocked::new(id, channel_to_client)
    }

    fn construct_as_player() -> PlayerMocked {
        construct_as_player_and(DEFAULT_PLAYER_ID, |_| {})
    }

    #[test]
    fn can_construct_as_player() {
        construct_as_player();
    }

    #[test]
    fn on_disconnected_when_in_no_session() {
        let player = construct_as_player();
        let player_id = player.id;
        let mut players = MockMyPlayerManager::new();
        players
            .expect_get_mut()
            .times(..)
            .with(eq(DEFAULT_PLAYER_ID))
            .return_var(player);
        let mut session_manager = MockMySessionManager::new();
        let mut players_info = MockMyPlayerInfoManager::new();
        let _players_need_update = PlayerMocked::on_disconnected(
            player_id,
            &mut players,
            &mut session_manager,
            &mut players_info,
        );
    }
}
