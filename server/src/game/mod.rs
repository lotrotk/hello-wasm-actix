pub mod messages;

mod definitions;
mod mines;
mod player;
mod pool;
mod session;

use std::sync::{Arc, Mutex};

pub use messages::*;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum GameType {
    Mines,
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct PlayerID(u32);
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct GameID(pub u32);
#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
#[must_use]
pub struct Score(pub i64);

pub trait PlayerPoolThread {}

pub trait PlayerPoolFront: Send {
    type Sender: SenderFromServerToClient;
    type Receiver;

    fn create_player(&mut self) -> Option<(Self::Receiver, PlayerToServer<Self::Sender>)>;
}

pub trait SenderFromServerToClient: std::fmt::Debug + Send {
    fn send(&mut self, msg: from_server::Message);
}

pub type SenderFromClientToServer<S> = std::sync::mpsc::Sender<pool::MessageToServer<S>>;

pub trait ServerToClientChannelFactory: Send + 'static {
    type Sender: SenderFromServerToClient;
    type Receiver;

    fn channel(&mut self) -> (Self::Sender, Self::Receiver);
}

pub struct PlayerToServer<S>
where
    S: SenderFromServerToClient,
{
    pub id: PlayerID,
    pub sender_from_client_to_server: SenderFromClientToServer<S>,
}

pub fn create_pool<F>(
    server_name: String,
    max_players: u32,
    max_sessions: u32,
    server_to_client_channel_factory: F,
) -> (
    Arc<
        Mutex<
            impl PlayerPoolFront<
                Receiver = <F as ServerToClientChannelFactory>::Receiver,
                Sender = <F as ServerToClientChannelFactory>::Sender,
            >,
        >,
    >,
    impl PlayerPoolThread,
)
where
    F: ServerToClientChannelFactory,
{
    pool::create_pool::<player::Player<<F as ServerToClientChannelFactory>::Sender>, F>(
        server_name,
        max_players,
        max_sessions,
        server_to_client_channel_factory,
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::fmt;

    use mockall::{automock, mock, predicate};
    use predicates::BoxPredicate;

    mock! {
        pub MySenderFromServerToClient {}

        impl SenderFromServerToClient for MySenderFromServerToClient {
            fn send(&mut self, msg: from_server::Message);
        }
    }

    impl fmt::Debug for MockMySenderFromServerToClient {
        fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(formatter, "MockMySenderFromServerToClient")
        }
    }

    type MockMyReceiverFromServerToClient = ();

    #[automock]
    pub trait MyServerToClientChannelFactory {
        fn channel(&mut self) -> MockMySenderFromServerToClient;
    }

    impl ServerToClientChannelFactory for MockMyServerToClientChannelFactory {
        type Sender = MockMySenderFromServerToClient;
        type Receiver = MockMyReceiverFromServerToClient;

        fn channel(&mut self) -> (Self::Sender, Self::Receiver) {
            let sender = MyServerToClientChannelFactory::channel(self);
            let receiver = ();
            (sender, receiver)
        }
    }

    const MAX_NUMBER_OF_PLAYERS: usize = 4;

    fn create_mocked_pool(
        expectations: impl FnOnce(&mut MockMyServerToClientChannelFactory),
    ) -> (
        Arc<
            Mutex<
                impl PlayerPoolFront<
                    Receiver = MockMyReceiverFromServerToClient,
                    Sender = MockMySenderFromServerToClient,
                >,
            >,
        >,
        impl PlayerPoolThread,
    ) {
        let server_name = "test_server".to_string();
        let max_players = MAX_NUMBER_OF_PLAYERS as u32;
        let max_sessions = 2;
        let mut server_to_client_channel_factory = MockMyServerToClientChannelFactory::new();
        expectations(&mut server_to_client_channel_factory);

        create_pool(
            server_name,
            max_players,
            max_sessions,
            server_to_client_channel_factory,
        )
    }

    #[derive(Copy, Clone)]
    struct NumberOfPlayers(usize);

    fn make_sender() -> MockMySenderFromServerToClient {
        use messages::from_server as fsm;

        let mut sender = MockMySenderFromServerToClient::new();
        let server_status_reply = |msg: &fsm::Message| {
            let status = match msg {
                fsm::Message::Status(status) => status,
                _ => return false,
            };
            status.sessions.is_empty()
        };
        let registration_reply = fsm::Message::PlayerRegistrationReply(
            fsm::PlayerRegistrationReply::RegisteredWithoutName,
        );

        let mut seq = mockall::Sequence::new();
        sender
            .expect_send()
            .once()
            .in_sequence(&mut seq)
            .with(predicate::function(server_status_reply))
            .return_const(());
        sender
            .expect_send()
            .once()
            .in_sequence(&mut seq)
            .with(predicate::eq(registration_reply))
            .return_const(());
        sender
    }

    fn expect_player_registrations(
        num_players: NumberOfPlayers,
        factory: &mut MockMyServerToClientChannelFactory,
        expectation_from_server: impl FnOnce(&mut MockMySenderFromServerToClient)
            + Clone
            + Sync
            + Send
            + 'static,
    ) {
        let mut seq = mockall::Sequence::new();
        let NumberOfPlayers(num_players) = num_players;
        factory
            .expect_channel()
            .times(num_players)
            .in_sequence(&mut seq)
            .returning(move || {
                let mut sender = make_sender();
                (expectation_from_server.clone())(&mut sender);
                sender
            });
    }

    fn expect_player_registration(
        factory: &mut MockMyServerToClientChannelFactory,
        expectation_from_server: impl FnOnce(&mut MockMySenderFromServerToClient)
            + Sync
            + Send
            + 'static,
    ) {
        factory.expect_channel().return_once(|| {
            let mut sender = make_sender();
            expectation_from_server(&mut sender);
            sender
        });
    }

    fn create_players_test(
        sleep: Sleep,
        num_players_created: NumberOfPlayers,
        num_players_not_created: NumberOfPlayers,
    ) {
        let (front, _thread) = create_mocked_pool(|factory| {
            expect_player_registrations(num_players_created, factory, |_| {})
        });
        let mut front = front.lock().unwrap();
        let NumberOfPlayers(num_players) = num_players_created;
        for _ in 0..num_players {
            assert!(front.create_player().is_some());
        }
        let NumberOfPlayers(num_players) = num_players_not_created;
        for _ in 0..num_players {
            assert!(front.create_player().is_none());
        }
        sleep.zzz();
    }

    fn create_one_player_test(
        sleep: Sleep,
        expectation_from_client: impl FnOnce(
            PlayerID,
            &SenderFromClientToServer<MockMySenderFromServerToClient>,
        ),
        expectation_from_server: impl FnOnce(&mut MockMySenderFromServerToClient)
            + Sync
            + Send
            + 'static,
    ) {
        let (front, _thread) = create_mocked_pool(|factory| {
            expect_player_registration(factory, expectation_from_server)
        });
        let (_receiver, player_to_server) = {
            let mut front = front.lock().unwrap();
            front.create_player().unwrap()
        };
        expectation_from_client(
            player_to_server.id,
            &player_to_server.sender_from_client_to_server,
        );
        sleep.zzz();
    }

    fn create_one_player_and_register_test(
        sleep: Sleep,
        expectation_from_client: impl FnOnce(
            PlayerID,
            &SenderFromClientToServer<MockMySenderFromServerToClient>,
        ),
        expectation_from_server: impl FnOnce(&mut MockMySenderFromServerToClient)
            + Sync
            + Send
            + 'static,
    ) {
        let name = "player_name";
        create_one_player_test(
            sleep,
            move |id, to_server| {
                send_to_server(
                    id,
                    to_server,
                    from_client::Content::PlayerRegistration { name: name.into() },
                );
                expectation_from_client(id, to_server)
            },
            move |to_client| {
                let rename_reply = from_server::Message::PlayerRegistrationReply(
                    from_server::PlayerRegistrationReply::RegisteredWithName(name.into()),
                );
                to_client
                    .expect_send()
                    .once()
                    .with(predicate::eq(rename_reply))
                    .return_const(());
                expectation_from_server(to_client)
            },
        )
    }

    fn create_one_player_test_directed(
        sleep: Sleep,
        messages_to_server: impl Iterator<Item = from_client::Content>,
        expectations_from_client: impl Iterator<Item = BoxPredicate<from_server::Message>>
            + Send
            + Sync
            + 'static,
    ) {
        create_one_player_test(
            sleep,
            move |id, to_server| {
                for content in messages_to_server {
                    send_to_server(id, to_server, content);
                }
            },
            move |to_client| {
                for exp in expectations_from_client {
                    to_client.expect_send().once().with(exp).return_const(());
                }
            },
        )
    }

    fn create_one_player_and_send_new_game_of_mines_message_and_fail_test(
        new_game: messages::mines::NewGame,
        reason: String,
    ) {
        let newgame_reply = {
            let new_game = new_game.clone();
            move |msg: &from_server::Message| match msg {
                from_server::Message::FailedToMakeGame {
                    title: msg_title,
                    reason: msg_reason,
                } => msg_title.eq(&new_game.title) && msg_reason.eq(&reason),
                _ => false,
            }
        };
        let from_client_content =
            messages::mines::from_client::Message::RequestNewGame(new_game).into();

        create_one_player_test_directed(
            Sleep(1),
            std::iter::once(from_client_content),
            std::iter::once(BoxPredicate::new(predicate::function(newgame_reply))),
        )
    }

    fn create_one_player_and_send_new_game_of_mines_message_test(
        sleep: Sleep,
        messages_to_server: impl Iterator<Item = from_client::Content>,
        expectations_from_client: impl Iterator<Item = BoxPredicate<from_server::Message>>
            + Send
            + Sync
            + 'static,
    ) {
        let title = "test session";
        let new_game = messages::mines::NewGame {
            width: 30,
            height: 30,
            mines: 100,
            title: title.into(),
        };

        let newgame_replies = {
            let new_game = new_game.clone();
            (
                move |msg: &from_server::Message| match msg {
                    from_server::Message::Mines { session, game } => {
                        session.eq(&GameID(0))
                            && game.eq(&messages::mines::from_server::Message::ConfirmNewGame(
                                new_game.clone(),
                            ))
                    }
                    _ => false,
                },
                |msg: &from_server::Message| match msg {
                    from_server::Message::Participation(from_server::Participation {
                        session,
                        player: _player,
                        player_is_you,
                    }) => session.eq(&GameID(0)) && *player_is_you,
                    _ => false,
                },
            )
        };
        let status_update_reply = move |msg: &from_server::Message| match msg {
            from_server::Message::Status(from_server::ServerStatus {
                name: _name,
                sessions,
            }) => {
                let mut it = sessions.iter();
                match it.next() {
                    Some((
                        id,
                        from_server::SessionDescription {
                            game_type,
                            title: t,
                        },
                    )) => {
                        id.eq(&GameID(0))
                            && game_type.eq(&GameType::Mines)
                            && t.eq(title)
                            && it.next().is_none()
                    }
                    _ => false,
                }
            }
            _ => false,
        };
        let from_client_content = {
            let new_mines_message = messages::mines::from_client::Message::RequestNewGame(new_game);
            let game_message = from_client::GameMessage::Mines(new_mines_message);
            from_client::Content::GameMessage(game_message)
        };

        let messages_to_server = std::iter::once(from_client_content).chain(messages_to_server);
        let expectations_from_client = vec![
            BoxPredicate::new(predicate::function(newgame_replies.0)),
            BoxPredicate::new(predicate::function(newgame_replies.1)),
            BoxPredicate::new(predicate::function(status_update_reply)),
        ]
        .into_iter()
        .chain(expectations_from_client);
        create_one_player_test_directed(sleep, messages_to_server, expectations_from_client)
    }

    fn send_to_server(
        player: PlayerID,
        to_server: &SenderFromClientToServer<MockMySenderFromServerToClient>,
        content: from_client::Content,
    ) {
        to_server
            .send(pool::MessageToServer::Public(from_client::Message(
                player, content,
            )))
            .unwrap();
    }

    struct Sleep(u32);

    impl Sleep {
        fn zzz(&self) {
            let Self(s) = self;
            let s = *s;
            std::thread::sleep(std::time::Duration::from_secs(s.into()))
        }
    }

    impl From<messages::mines::from_client::Message> for from_client::Content {
        fn from(mines_message: messages::mines::from_client::Message) -> Self {
            let game_message = from_client::GameMessage::Mines(mines_message);
            Self::GameMessage(game_message)
        }
    }

    #[test]
    fn construction() {
        let (_front, _thread) = create_mocked_pool(|_| {});
        Sleep(1).zzz();
    }

    #[test]
    fn create_one_player() {
        create_players_test(Sleep(1), NumberOfPlayers(1), NumberOfPlayers(0))
    }

    #[test]
    fn create_max_number_of_players() {
        create_players_test(
            Sleep(1),
            NumberOfPlayers(MAX_NUMBER_OF_PLAYERS),
            NumberOfPlayers(0),
        )
    }

    #[test]
    fn create_too_much_players() {
        create_players_test(
            Sleep(1),
            NumberOfPlayers(MAX_NUMBER_OF_PLAYERS),
            NumberOfPlayers(3),
        )
    }

    #[test]
    fn create_one_player_and_register() {
        create_one_player_and_register_test(Sleep(1), |_, _| {}, |_| {})
    }

    #[test]
    fn create_one_player_and_quit_game() {
        create_one_player_and_register_test(
            Sleep(1),
            |id, to_server| {
                send_to_server(id, to_server, from_client::Content::QuitGame(GameID(666)));
            },
            |_| {},
        ) //it should just be ignored
    }

    #[test]
    fn create_one_player_and_register_and_quit_game() {
        create_one_player_and_register_test(
            Sleep(1),
            |id, to_server| {
                send_to_server(id, to_server, from_client::Content::QuitGame(GameID(666)));
            },
            |_| {},
        ) //it should just be ignored
    }

    #[test]
    fn create_one_player_and_register_and_disconnect() {
        create_one_player_and_register_test(
            Sleep(3),
            |id, to_server| {
                send_to_server(id, to_server, from_client::Content::Disconnected);
            },
            |_| {},
        )
    }

    #[test]
    fn create_one_player_and_register_and_disconnect_twice() {
        create_one_player_and_register_test(
            Sleep(3),
            |id, to_server| {
                for _ in 0..2 {
                    send_to_server(id, to_server, from_client::Content::Disconnected);
                }
            },
            |_| {},
        )
    }

    #[test]
    fn create_one_player_and_send_game_message() {
        create_one_player_and_register_test(
            Sleep(1),
            |id, to_server| {
                let game_message = messages::mines::from_client::Message::Marking(
                    GameID(666),
                    messages::mines::Coordinate { x: 0, y: 0 },
                    messages::mines::from_client::CellMarking::Safe,
                );
                send_to_server(
                    id,
                    to_server,
                    from_client::Content::GameMessage(from_client::GameMessage::Mines(
                        game_message,
                    )),
                );
            },
            |_| {},
        ) //it should just be ignored
    }

    #[test]
    fn create_one_player_and_send_new_game_of_mines_message_with_too_few_mines() {
        let title = "test session";
        let new_game = messages::mines::NewGame {
            width: 30,
            height: 30,
            mines: 40,
            title: title.into(),
        };
        create_one_player_and_send_new_game_of_mines_message_and_fail_test(
            new_game,
            "Not enough mines".into(),
        );
    }

    #[test]
    fn create_one_player_and_send_new_game_of_mines_message_with_too_little_height() {
        let title = "test session";
        let new_game = messages::mines::NewGame {
            width: 30,
            height: 5,
            mines: 100,
            title: title.into(),
        };
        create_one_player_and_send_new_game_of_mines_message_and_fail_test(
            new_game,
            "Not high enough".into(),
        );
    }

    #[test]
    fn create_one_player_and_send_new_game_of_mines_message_with_too_little_width() {
        let title = "test session";
        let new_game = messages::mines::NewGame {
            width: 5,
            height: 30,
            mines: 100,
            title: title.into(),
        };
        create_one_player_and_send_new_game_of_mines_message_and_fail_test(
            new_game,
            "Not wide enough".into(),
        );
    }

    #[test]
    fn create_one_player_and_send_new_game_of_mines_message_which_is_fine() {
        create_one_player_and_send_new_game_of_mines_message_test(
            Sleep(1),
            std::iter::empty(),
            std::iter::empty(),
        );
    }

    #[test]
    fn with_one_player_and_one_mines_game_send_gameaction_with_invalid_sessionid_is_ignored() {
        let marking = messages::mines::from_client::CellMarking::Clear;
        let coordinate = messages::mines::Coordinate { x: 0, y: 0 };

        let id = GameID(666);
        let content =
            messages::mines::from_client::Message::Marking(id, coordinate, marking).into();
        create_one_player_and_send_new_game_of_mines_message_test(
            Sleep(1),
            std::iter::once(content),
            std::iter::empty(),
        );
    }

    #[test]
    fn with_one_player_and_one_mines_game_send_gameaction_with_valid_sessionid_gets_reply() {
        let marking = messages::mines::from_client::CellMarking::Clear;
        let coordinate = messages::mines::Coordinate { x: 0, y: 0 };
        let id = GameID(0);

        let content =
            messages::mines::from_client::Message::Marking(id, coordinate, marking).into();

        let reply =
            BoxPredicate::new(predicate::function(
                move |msg: &from_server::Message| match msg {
                    from_server::Message::Mines {
                        session,
                        game: messages::mines::from_server::Message::Marking(_marking),
                    } => session.eq(&id),
                    _ => false,
                },
            ));

        create_one_player_and_send_new_game_of_mines_message_test(
            Sleep(1),
            std::iter::once(content),
            std::iter::once(reply),
        );
    }
}
