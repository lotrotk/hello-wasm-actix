use super::*;

pub struct PlayerData {
    players: HashMap<PlayerID, StatisticsPerPlayer>,
    state: GameState,
}

impl PlayerData {
    pub fn new() -> Self {
        let state = GameState::Unfinished;
        let players = HashMap::new();
        Self { players, state }
    }

    pub fn update_player_stats(&mut self, player: PlayerID, stats: &Statistics) {
        let pstats = self
            .players
            .get_mut(&player)
            .expect("This game does not have that player");
        pstats.add(stats);
    }

    pub fn update_state(
        &mut self,
        player: PlayerID,
        indication: internal::GameStateIndication,
    ) -> Option<Finished> {
        let (finished, new_state) = match indication {
            internal::GameStateIndication::Unfinished => {
                let finished = None;
                (finished, GameState::Unfinished)
            }
            internal::GameStateIndication::Won => {
                let finished = Some(Finished {
                    player_that_did_boom: None,
                    stats: self.players.clone(),
                });
                (finished, GameState::Won)
            }
            internal::GameStateIndication::Boom => {
                let finished = Some(Finished {
                    player_that_did_boom: Some(player),
                    stats: self.players.clone(),
                });
                (finished, GameState::BoomBy(player))
            }
        };
        self.state = new_state;
        finished
    }

    pub fn stats(&self) -> impl Iterator<Item = (PlayerID, &StatisticsPerPlayer)> {
        self.players.iter().map(|(player, stats)| (*player, stats))
    }
}

impl session::GamePlayerAccess for PlayerData {
    fn insert_player(&mut self, player: PlayerID, _: session::GamePlayerAccessKey) {
        let previous_player = self.players.insert(player, StatisticsPerPlayer::default());
        assert!(previous_player.is_none(), "This player is'nt new")
    }

    fn remove_player(&mut self, player: PlayerID, _: session::GamePlayerAccessKey) -> Score {
        let stats = self
            .players
            .remove(&player)
            .expect("Could not remove player from session");
        stats.score()
    }
}

enum GameState {
    Unfinished,
    Won,
    BoomBy(PlayerID),
}
