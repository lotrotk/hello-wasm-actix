use super::*;

pub enum GameStateIndication {
    Unfinished,
    Won,
    Boom,
}

pub enum CellsInfoUpdate {
    None,
    One(Index, CellInfo),
    Some(HashMap<Index, CellInfo>),
    All(Vec<CellInfo>),
}
