use super::*;

const MIN_WIDTH: u32 = 15;
const MAX_WIDTH: u32 = 100;
const MIN_HEIGHT: u32 = 15;
const MAX_HEIGHT: u32 = 150;
const MIN_MINES_PER_AREA_RATIO: f32 = 0.05;
const MAX_MINES_PER_AREA_RATIO: f32 = 0.5;

pub struct CommonData {
    width: u32,
    _height: u32,
    total_mines: u32,
    stats: CommonDataStats,
    field: Vec<Cell>,
}

impl CommonData {
    pub fn try_new(width: u32, height: u32, total_mines: u32) -> Result<Self, &'static str> {
        if width < MIN_WIDTH {
            return Err("Not wide enough");
        }
        if width > MAX_WIDTH {
            return Err("Too wide");
        }
        if height < MIN_HEIGHT {
            return Err("Not high enough");
        }
        if height > MAX_HEIGHT {
            return Err("Too high");
        }
        let area = width * height;
        let mines_per_area_ratio = (total_mines as f32) / (area as f32);
        if mines_per_area_ratio < MIN_MINES_PER_AREA_RATIO {
            return Err("Not enough mines");
        }
        if mines_per_area_ratio > MAX_MINES_PER_AREA_RATIO {
            return Err("Too many mines");
        }

        let mut field = vec![Cell::default(); area as usize];
        fill(field.as_mut_slice(), total_mines);

        let stats = CommonDataStats::default();
        Ok(Self {
            width,
            _height: height,
            total_mines,
            stats,
            field,
        })
    }

    #[allow(dead_code)]
    pub fn try_new_with_field(
        width: u32,
        field: impl Iterator<Item = Mine> + Clone,
    ) -> Result<Self, &'static str> {
        let length = field.clone().count();
        let height = {
            if length % width as usize != 0 {
                return Err("Invalid width");
            }
            length as u32 / width
        };
        let total_mines = field.clone().filter(|mine| *mine == Mine::Yes).count() as u32;
        let field = field
            .map(|mine| Cell {
                mine,
                covery: Covery::Covered,
            })
            .collect();
        let stats = CommonDataStats::default();
        Ok(Self {
            width,
            _height: height,
            total_mines,
            stats,
            field,
        })
    }

    pub fn get_stats(&self) -> &CommonDataStats {
        &self.stats
    }

    pub fn get_total_mines(&self) -> u32 {
        self.total_mines
    }

    fn cell_at(&self, index: Index) -> Result<&Cell, &'static str> {
        let Index(index) = index;
        self.field
            .get(index as usize)
            .ok_or("Coordinates out of range")
    }

    fn mut_cell_at(&mut self, index: Index) -> Result<&mut Cell, &'static str> {
        Self::get_mut_cell_at(&mut self.field, index)
    }

    fn get_mut_cell_at(field: &mut Vec<Cell>, index: Index) -> Result<&mut Cell, &'static str> {
        let Index(index) = index;
        field
            .get_mut(index as usize)
            .ok_or("Coordinates out of range")
    }

    pub fn transition(
        &mut self,
        index: Index,
        marking: Covery,
        winner_condition: impl Fn(&Statistics) -> bool,
    ) -> Result<
        (
            internal::GameStateIndication,
            internal::CellsInfoUpdate,
            Statistics,
        ),
        &'static str,
    > {
        enum Indication {
            Unknown,
            NeedsClearing,
            Boom,
        }

        let mut stats = Statistics::default();
        stats.actions += 1;
        let cell = Self::get_mut_cell_at(&mut self.field, index).unwrap();
        let no_action = |stats: &mut Statistics| {
            stats.actions -= 1;
            Indication::Unknown
        };
        let indication = match (cell.covery, marking, cell.mine) {
            (Covery::Uncovered, Covery::Uncovered, _) => Indication::Unknown,
            (Covery::Uncovered, Covery::Covered, _)
            | (Covery::Uncovered, Covery::Marked, _)
            | (Covery::Uncovered, Covery::Questioned, _) => {
                return Err("This cell is already uncovered");
            }
            (Covery::Covered, Covery::Uncovered, Mine::Yes) => Indication::Boom,
            (Covery::Covered, Covery::Covered, _) => no_action(&mut stats),
            (Covery::Covered, Covery::Uncovered, Mine::No) => Indication::NeedsClearing,
            (Covery::Covered, Covery::Marked, Mine::Yes) => {
                stats.mines_marked_correctly += 1;
                Indication::Unknown
            }
            (Covery::Covered, Covery::Marked, Mine::No) => {
                stats.mines_marked_incorrectly += 1;
                Indication::Unknown
            }
            (Covery::Covered, Covery::Questioned, _) => {
                stats.questioned += 1;
                Indication::Unknown
            }
            (Covery::Marked, Covery::Uncovered, Mine::Yes) => {
                stats.cleared_correct_markings += 1;
                Indication::Boom
            }
            (Covery::Marked, Covery::Uncovered, Mine::No) => {
                stats.cleared_incorrect_markings += 1;
                Indication::NeedsClearing
            }
            (Covery::Marked, Covery::Covered, Mine::Yes) => {
                stats.cleared_correct_markings += 1;
                Indication::Unknown
            }
            (Covery::Marked, Covery::Covered, Mine::No) => {
                stats.cleared_incorrect_markings += 1;
                Indication::Unknown
            }
            (Covery::Marked, Covery::Marked, _) => no_action(&mut stats),
            (Covery::Marked, Covery::Questioned, Mine::Yes) => {
                stats.cleared_correct_markings += 1;
                stats.questioned += 1;
                Indication::Unknown
            }
            (Covery::Marked, Covery::Questioned, Mine::No) => {
                stats.cleared_incorrect_markings += 1;
                stats.questioned += 1;
                Indication::Unknown
            }
            (Covery::Questioned, Covery::Uncovered, Mine::Yes) => {
                stats.cleared_questioned += 1;
                Indication::Boom
            }
            (Covery::Questioned, Covery::Uncovered, Mine::No) => {
                //questioned stats are adjusted eventually
                Indication::NeedsClearing
            }
            (Covery::Questioned, Covery::Covered, _) => {
                stats.cleared_questioned += 1;
                Indication::Unknown
            }
            (Covery::Questioned, Covery::Marked, Mine::Yes) => {
                stats.mines_marked_correctly += 1;
                stats.cleared_questioned += 1;
                Indication::Unknown
            }
            (Covery::Questioned, Covery::Marked, Mine::No) => {
                stats.mines_marked_incorrectly += 1;
                stats.cleared_questioned += 1;
                Indication::Unknown
            }
            (Covery::Questioned, Covery::Questioned, _) => no_action(&mut stats),
        };
        self.stats.update(&stats);
        cell.covery = match indication {
            Indication::Boom | Indication::Unknown => marking,
            Indication::NeedsClearing => cell.covery,
        };
        let (state, update) = match indication {
            Indication::Boom => {
                let mut cell_info = self.show_all(None);
                cell_info[index.0 as usize] = CellInfo::MineThatDidBoom;
                (
                    internal::GameStateIndication::Boom,
                    internal::CellsInfoUpdate::All(cell_info),
                )
            }
            Indication::NeedsClearing => {
                if winner_condition(&stats) {
                    let cell_info = self.show_all(Some(&mut stats));
                    (
                        internal::GameStateIndication::Boom,
                        internal::CellsInfoUpdate::All(cell_info),
                    )
                } else {
                    let cell_info = self.uncover_neighbors(index, &mut stats);
                    let cell_info = if cell_info.is_empty() {
                        internal::CellsInfoUpdate::None
                    } else if cell_info.len() == 1 {
                        let (index, info) = cell_info.into_iter().next().unwrap();
                        internal::CellsInfoUpdate::One(index, info)
                    } else {
                        internal::CellsInfoUpdate::Some(cell_info)
                    };
                    (internal::GameStateIndication::Unfinished, cell_info)
                }
            }
            Indication::Unknown => {
                if winner_condition(&stats) {
                    let cell_info = self.show_all(Some(&mut stats));
                    (
                        internal::GameStateIndication::Won,
                        internal::CellsInfoUpdate::All(cell_info),
                    )
                } else {
                    let info = marking.try_into().unwrap();
                    (
                        internal::GameStateIndication::Unfinished,
                        internal::CellsInfoUpdate::One(index, info),
                    )
                }
            }
        };
        Ok((state, update, stats))
    }

    fn show_neighbor_info_at_no_mine(&self, index: Index) -> CellInfo {
        CellInfo::NeighboringMines(index.neighbors(self, IncludeCorners::Yes).fold(
            0,
            |number, neighbor| {
                let neighbor = self.cell_at(neighbor).unwrap();
                match neighbor.mine {
                    Mine::Yes => number + 1,
                    Mine::No => number,
                }
            },
        ))
    }

    fn show_info_at_no_mine(&self, index: Index, previous: Covery) -> CellInfo {
        match previous {
            Covery::Marked => CellInfo::MarkedIncorrectly,
            Covery::Uncovered | Covery::Covered | Covery::Questioned => {
                self.show_neighbor_info_at_no_mine(index)
            }
        }
    }

    fn show_info_at(&self, index: Index, is_mine: Mine, previous: Covery) -> CellInfo {
        match is_mine {
            Mine::Yes => match previous {
                Covery::Uncovered => CellInfo::Mine,
                Covery::Covered => CellInfo::Mine,
                Covery::Marked => CellInfo::MarkedCorrectly,
                Covery::Questioned => CellInfo::Mine,
            },
            Mine::No => self.show_info_at_no_mine(index, previous),
        }
    }

    fn uncover(cell: &mut Cell, stats: &mut Statistics) -> bool {
        if Mine::Yes == cell.mine {
            return false;
        }
        match cell.covery {
            Covery::Uncovered | Covery::Marked => return false,
            Covery::Questioned => stats.cleared_questioned += 1,
            Covery::Covered => {}
        }
        cell.covery = Covery::Uncovered;
        true
    }

    fn show_all(&mut self, stats: Option<&mut Statistics>) -> Vec<CellInfo> {
        let mut dummy_stats = Statistics::default();
        let stats = stats.unwrap_or(&mut dummy_stats);
        for cell in &mut self.field {
            Self::uncover(cell, stats);
        }
        self.field
            .iter()
            .enumerate()
            .map(|(index, cell): (usize, &Cell)| {
                let index = Index(index as u32);
                self.show_info_at(index, cell.mine, cell.covery)
            })
            .collect()
    }

    fn uncover_neighbors(
        &mut self,
        index: Index,
        stats: &mut Statistics,
    ) -> HashMap<Index, CellInfo> {
        let mut updated = HashMap::new();
        let mut processed = HashSet::new();
        let mut to_be_updated = HashSet::new();
        to_be_updated.insert(index);
        while let Some(index) = to_be_updated.iter().cloned().next() {
            to_be_updated.remove(&index);
            processed.insert(index);
            if !Self::uncover(self.mut_cell_at(index).unwrap(), stats) {
                continue;
            }
            for neighbor in index
                .neighbors(self, IncludeCorners::No)
                .filter(|n| !processed.contains(n))
            {
                to_be_updated.insert(neighbor);
            }
            let updated_info = self.show_neighbor_info_at_no_mine(index);
            updated.insert(index, updated_info);
        }
        updated
    }
}

#[derive(Clone, Copy, Default)]
pub struct CommonDataStats {
    pub mines_marked_correctly: u32,
    pub mines_marked_incorrectly: u32,
}

impl CommonDataStats {
    pub fn update(&mut self, stats: &StatisticsPerPlayer) {
        self.mines_marked_correctly += stats.mines_marked_correctly;
        self.mines_marked_correctly -= stats.cleared_correct_markings;
        self.mines_marked_incorrectly += stats.mines_marked_incorrectly;
        self.mines_marked_incorrectly -= stats.cleared_incorrect_markings;
    }
}

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub struct Index(u32);

impl Index {
    pub fn from_coordinates_yx(session: &Session, y: u32, x: u32) -> Self {
        Index(y * session.common.width + x)
    }

    pub fn to_coordinates_yx(self, session: &Session) -> (u32, u32) {
        let Self(index) = self;
        let y = index / session.common.width;
        let x = index % session.common.width;
        (y, x)
    }

    fn neighbors(
        &self,
        common: &CommonData,
        include_corners: IncludeCorners,
    ) -> impl Iterator<Item = Index> {
        #[derive(Clone, Copy)]
        enum PointingTo {
            TopLeft,
            Top,
            TopRight,
            Left,
            Right,
            BottomLeft,
            Bottom,
            BottomRight,
        }

        struct Iter {
            center: i32,
            width: i32,
            area: i32,
            corners: IncludeCorners,
            point: Option<PointingTo>,
        }

        impl Iter {
            fn new(index: Index, common: &CommonData, corners: IncludeCorners) -> Self {
                let Index(center) = index;
                Iter {
                    center: center as i32,
                    width: common.width as i32,
                    area: common.field.len() as i32,
                    corners,
                    point: Some(PointingTo::TopLeft),
                }
            }

            fn diff(&self, d: i32) -> Option<Index> {
                let i = self.center + d;
                if 0 <= i && i < self.area {
                    let x0 = self.center % self.width;
                    let x1 = i % self.width;
                    if i32::abs(x0 - x1) <= 1 {
                        return Some(Index(i as u32));
                    }
                }
                None
            }
        }

        impl Iterator for Iter {
            type Item = Index;

            fn next(&mut self) -> Option<Self::Item> {
                let current_point = match &self.point {
                    Some(point) => *point,
                    None => return None,
                };
                let next_point = match current_point {
                    PointingTo::TopLeft => Some(PointingTo::Top),
                    PointingTo::Top => Some(PointingTo::TopRight),
                    PointingTo::TopRight => Some(PointingTo::Left),
                    PointingTo::Left => Some(PointingTo::Right),
                    PointingTo::Right => Some(PointingTo::BottomLeft),
                    PointingTo::BottomLeft => Some(PointingTo::Bottom),
                    PointingTo::Bottom => Some(PointingTo::BottomRight),
                    PointingTo::BottomRight => None,
                };
                self.point = next_point;
                if let IncludeCorners::No = self.corners {
                    match current_point {
                        PointingTo::TopLeft
                        | PointingTo::TopRight
                        | PointingTo::BottomLeft
                        | PointingTo::BottomRight => return self.next(),
                        _ => {}
                    }
                }
                let width = self.width;
                let current_index = match current_point {
                    PointingTo::TopLeft => self.diff(-width - 1),
                    PointingTo::Top => self.diff(-width),
                    PointingTo::TopRight => self.diff(-width + 1),
                    PointingTo::Left => self.diff(-1),
                    PointingTo::Right => self.diff(1),
                    PointingTo::BottomLeft => self.diff(width - 1),
                    PointingTo::Bottom => self.diff(width),
                    PointingTo::BottomRight => self.diff(width + 1),
                };
                current_index.or_else(|| self.next())
            }
        }

        Iter::new(*self, common, include_corners)
    }
}

pub fn index_from_number(number: u32) -> Index {
    Index(number)
}

#[derive(Clone)]
struct Cell {
    mine: Mine,
    covery: Covery,
}

impl Default for Cell {
    fn default() -> Self {
        Self {
            mine: Mine::No,
            covery: Covery::Covered,
        }
    }
}

fn fill(field: &mut [Cell], total_mines: u32) {
    for _ in 0..total_mines {
        loop {
            let index = rand::random::<usize>() % field.len();
            let cell = &mut field[index];
            if let Mine::No = cell.mine {
                cell.mine = Mine::Yes;
                break;
            }
        }
    }
}

#[derive(Clone, Copy)]
enum IncludeCorners {
    Yes,
    No,
}
