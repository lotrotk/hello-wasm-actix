mod common_data;
mod internal;
mod player_data;

use std::collections::{HashMap, HashSet};
use std::convert::TryInto;

use super::*;

use common_data::CommonData;
use player_data::PlayerData;

pub use common_data::Index;

const SCORE_MULTIPLIER_FOR_MARKED_MINES: i64 = 1;

pub struct Session {
    common: CommonData,
    players: PlayerData,
}

impl Session {
    pub fn try_new(width: u32, height: u32, total_mines: u32) -> Result<Self, &'static str> {
        let common = CommonData::try_new(width, height, total_mines)?;
        let players = PlayerData::new();
        Ok(Self { common, players })
    }

    #[allow(dead_code)]
    fn try_new_with_field(
        width: u32,
        field: impl Iterator<Item = bool> + Clone,
    ) -> Result<Self, &'static str> {
        let field = field.map(|is_mine| match is_mine {
            true => Mine::Yes,
            false => Mine::No,
        });
        let common = CommonData::try_new_with_field(width, field)?;
        let players = PlayerData::new();
        Ok(Self { common, players })
    }

    pub fn mark(&mut self, player: PlayerID, index: Index, marking: Covery) -> MarkReply {
        let transition = {
            let mines_already_marked_correctly = self.common.get_stats().mines_marked_correctly;
            let mines_already_marked_incorrectly = self.common.get_stats().mines_marked_incorrectly;
            let total_mines = self.common.get_total_mines();
            let winner_condition = move |stats: &Statistics| {
                let mines_marked_correctly = mines_already_marked_correctly
                    + stats.mines_marked_correctly
                    - stats.cleared_correct_markings;
                let mines_marked_incorrectly = mines_already_marked_incorrectly
                    + stats.mines_marked_incorrectly
                    - stats.cleared_incorrect_markings;
                mines_marked_correctly == total_mines && mines_marked_incorrectly == 0
            };
            self.common.transition(index, marking, winner_condition)
        };
        let (indication, update, stats) = match transition {
            Err(msg) => {
                return MarkReply::from_error_message(msg);
            }
            Ok(result) => result,
        };

        self.players.update_player_stats(player, &stats);
        let finished = self.players.update_state(player, indication);
        let update = CellsInfoUpdate(update);
        MarkReply::new(update, finished)
    }

    pub fn stats(&self) -> impl Iterator<Item = (PlayerID, &StatisticsPerPlayer)> {
        self.players.stats()
    }
}

impl session::GamePlayerAccess for Session {
    fn insert_player(&mut self, player: PlayerID, key: session::GamePlayerAccessKey) {
        self.players.insert_player(player, key)
    }

    fn remove_player(&mut self, player: PlayerID, key: session::GamePlayerAccessKey) -> Score {
        self.players.remove_player(player, key)
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum CellInfo {
    Covered,
    MarkedCorrectly,
    MarkedEitherCorrectlyOrIncorrectly,
    MarkedIncorrectly,
    Mine,
    MineThatDidBoom,
    NeighboringMines(u8),
    Questioned,
}

impl std::convert::TryFrom<Covery> for CellInfo {
    type Error = &'static str;

    fn try_from(value: Covery) -> Result<Self, Self::Error> {
        let info = match value {
            Covery::Uncovered => {
                return Err("Unknown number of neighboring mines");
            }
            Covery::Covered => CellInfo::Covered,
            Covery::Marked => CellInfo::MarkedEitherCorrectlyOrIncorrectly,
            Covery::Questioned => CellInfo::Questioned,
        };
        Ok(info)
    }
}

pub struct CellsInfoUpdate(internal::CellsInfoUpdate);

impl CellsInfoUpdate {
    pub fn iter(&self) -> CellsInfoUpdateIterator<'_> {
        match &self.0 {
            internal::CellsInfoUpdate::None => CellsInfoUpdateIterator::None,
            internal::CellsInfoUpdate::One(index, info) => {
                CellsInfoUpdateIterator::One(std::iter::once((*index, info)))
            }
            internal::CellsInfoUpdate::Some(map) => CellsInfoUpdateIterator::Map(map.iter()),
            internal::CellsInfoUpdate::All(vec) => {
                CellsInfoUpdateIterator::Slice(vec.iter().enumerate())
            }
        }
    }
}

impl<'a> IntoIterator for &'a CellsInfoUpdate {
    type IntoIter = CellsInfoUpdateIterator<'a>;
    type Item = <Self::IntoIter as Iterator>::Item;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

pub enum CellsInfoUpdateIterator<'a> {
    None,
    One(std::iter::Once<(Index, &'a CellInfo)>),
    Slice(std::iter::Enumerate<std::slice::Iter<'a, CellInfo>>),
    Map(std::collections::hash_map::Iter<'a, Index, CellInfo>),
}

impl<'a> Iterator for CellsInfoUpdateIterator<'a> {
    type Item = (Index, &'a CellInfo);

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            Self::None => None,
            Self::One(it) => it.next(),
            Self::Slice(it) => it
                .next()
                .map(|(index, item)| (common_data::index_from_number(index as u32), item)),
            Self::Map(it) => it.next().map(|(index, item)| (*index, item)),
        }
    }
}

impl<'a> ExactSizeIterator for CellsInfoUpdateIterator<'a> {
    fn len(&self) -> usize {
        match self {
            Self::None => 0,
            Self::One(_) => 1,
            Self::Slice(it) => it.len(),
            Self::Map(it) => it.len(),
        }
    }
}

pub struct Finished {
    pub player_that_did_boom: Option<PlayerID>,
    pub stats: HashMap<PlayerID, StatisticsPerPlayer>,
}

pub enum MarkReply {
    Invalid(&'static str),
    Valid {
        update: CellsInfoUpdate,
        finished: Option<Finished>,
    },
}

impl MarkReply {
    fn from_error_message(msg: &'static str) -> Self {
        Self::Invalid(msg)
    }

    fn new(update: CellsInfoUpdate, finished: Option<Finished>) -> Self {
        Self::Valid { update, finished }
    }
}

#[derive(Clone, Copy, PartialEq)]
pub enum Mine {
    Yes,
    No,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Covery {
    Uncovered,
    Covered,
    Marked,
    Questioned,
}

#[derive(Clone, Default)]
pub struct Statistics {
    pub mines_marked_correctly: u32,
    pub mines_marked_incorrectly: u32,
    pub cleared_correct_markings: u32,
    pub cleared_incorrect_markings: u32,
    pub actions: u32,
    pub questioned: u32,
    pub cleared_questioned: u32,
}

type StatisticsPerPlayer = Statistics;

impl Statistics {
    pub fn score(&self) -> Score {
        let positive = SCORE_MULTIPLIER_FOR_MARKED_MINES * self.mines_marked_correctly as i64
            + self.cleared_incorrect_markings as i64;
        let negative = SCORE_MULTIPLIER_FOR_MARKED_MINES * self.mines_marked_incorrectly as i64
            + self.cleared_correct_markings as i64;
        Score(positive - negative)
    }

    fn add(&mut self, stats: &Self) {
        let updated = Self {
            mines_marked_correctly: self.mines_marked_correctly + stats.mines_marked_correctly,
            mines_marked_incorrectly: self.mines_marked_incorrectly
                + stats.mines_marked_incorrectly,
            cleared_correct_markings: self.cleared_correct_markings
                + stats.cleared_correct_markings,
            cleared_incorrect_markings: self.cleared_incorrect_markings
                + stats.cleared_incorrect_markings,
            actions: self.actions + stats.actions,
            questioned: self.questioned + stats.questioned,
            cleared_questioned: self.cleared_questioned + stats.cleared_questioned,
        };
        *self = updated;
    }
}

#[cfg(test)]
use common_data::CommonDataStats;
#[cfg(test)]
use session::GamePlayerAccess;

#[cfg(test)]
pub fn new_session_with_field(width: u32, field: impl Iterator<Item = bool> + Clone) -> Session {
    Session::try_new_with_field(width, field).unwrap()
}

#[cfg(test)]
#[derive(Clone, Copy, Default)]
pub struct CommonDataStatsTest {
    mines_marked_correctly: u32,
    mines_marked_incorrectly: u32,
}

#[cfg(test)]
impl From<CommonDataStats> for CommonDataStatsTest {
    fn from(stats: CommonDataStats) -> Self {
        Self {
            mines_marked_correctly: stats.mines_marked_correctly,
            mines_marked_incorrectly: stats.mines_marked_incorrectly,
        }
    }
}

#[cfg(test)]
pub fn get_common_data_stats(session: &Session) -> CommonDataStatsTest {
    session.common.get_stats().clone().into()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::player::tests::DEFAULT_PLAYER_ID;

    const WIDTH: u32 = 5;
    const HEIGHT: u32 = 5;
    // xx000
    // x0xxx
    // xxx00
    // x0x0x
    // x0000
    static MINES: [(u32, u32); 13] = [
        (0, 0),
        (0, 1),
        (1, 0),
        (1, 2),
        (1, 3),
        (1, 4),
        (2, 0),
        (2, 1),
        (2, 2),
        (3, 0),
        (3, 2),
        (3, 4),
        (4, 0),
    ];

    fn iter_not_mines() -> impl Iterator<Item = (u32, u32)> {
        (0..HEIGHT)
            .zip(0..WIDTH)
            .filter(|c| MINES.iter().find(|coordinate| *coordinate == c).is_none())
    }

    fn create_session() -> Session {
        let mut field = [false; (WIDTH * HEIGHT) as usize];
        for (y, x) in &MINES {
            field[(y * WIDTH + x) as usize] = true;
        }
        let mut session = new_session_with_field(WIDTH, field.iter().cloned());
        session.insert_player(
            DEFAULT_PLAYER_ID,
            session::GamePlayerAccessKey::new_for_test(),
        );
        session
    }

    fn mark_valid(
        session: &mut Session,
        index: Index,
        covery: Covery,
    ) -> (CellsInfoUpdate, Option<Finished>) {
        match session.mark(DEFAULT_PLAYER_ID, index, covery) {
            MarkReply::Invalid(msg) => panic!("{}", msg),
            MarkReply::Valid { update, finished } => (update, finished),
        }
    }

    fn find_and_do(update: &CellsInfoUpdate, index: Index, functor: impl Fn(&CellInfo)) {
        let info = match update.iter().find(|(i, _)| *i == index) {
            Some((_, info)) => info,
            None => panic!("Index not in update"),
        };
        functor(info);
    }

    #[test]
    fn construct_session() {
        let _ = create_session();
    }

    #[test]
    fn uncover_empty() {
        let mut session = create_session();
        let index = Index::from_coordinates_yx(&session, 1, 1);
        let (update, finished) = mark_valid(&mut session, index, Covery::Uncovered);
        assert!(finished.is_none());
        find_and_do(&update, index, |info| {
            assert_eq!(*info, CellInfo::NeighboringMines(7))
        });
        assert_eq!(update.iter().count(), 1);
    }

    #[test]
    fn uncover_empty_area() {
        let mut session = create_session();
        let index = Index::from_coordinates_yx(&session, 0, 4);
        let (update, finished) = mark_valid(&mut session, index, Covery::Uncovered);
        assert!(finished.is_none());
        let space = [(0, 2, 3), (0, 3, 3), (0, 4, 2)];
        for (y, x, mines) in &space {
            let index = Index::from_coordinates_yx(&session, *y, *x);
            find_and_do(&update, index, |info| {
                assert_eq!(*info, CellInfo::NeighboringMines(*mines))
            });
        }
        assert_eq!(update.iter().count(), space.len());
    }

    #[test]
    fn boom() {
        let mut session = create_session();
        let (y, x) = MINES.iter().nth(0).unwrap();
        let index = Index::from_coordinates_yx(&session, *y, *x);
        let (update, finished) = mark_valid(&mut session, index, Covery::Uncovered);

        // finished
        let finished = match finished {
            Some(finished) => finished,
            None => panic!("Not finished"),
        };
        match finished.player_that_did_boom {
            Some(player) if player == DEFAULT_PLAYER_ID => {}
            Some(_) => panic!("Wrong player did boom"),
            None => panic!("No boom"),
        }
        let stats = &finished.stats[&DEFAULT_PLAYER_ID];
        assert_eq!(stats.mines_marked_correctly, 0);
        assert_eq!(stats.mines_marked_incorrectly, 0);
        assert_eq!(stats.cleared_correct_markings, 0);
        assert_eq!(stats.cleared_incorrect_markings, 0);
        assert_eq!(stats.actions, 1);
        assert_eq!(stats.questioned, 0);
        assert_eq!(stats.cleared_questioned, 0);

        // update
        let index = Index::from_coordinates_yx(&session, *y, *x);
        find_and_do(&update, index, |info| {
            assert_eq!(*info, CellInfo::MineThatDidBoom)
        });
        for (y, x) in MINES.iter().skip(1) {
            let index = Index::from_coordinates_yx(&session, *y, *x);
            find_and_do(&update, index, |info| assert_eq!(*info, CellInfo::Mine));
        }
        for (y, x) in iter_not_mines() {
            let index = Index::from_coordinates_yx(&session, y, x);
            find_and_do(&update, index, |info| match info {
                CellInfo::NeighboringMines(_) => {}
                _ => panic!("This is not an empty space"),
            });
        }
        assert_eq!(update.iter().count(), (WIDTH * HEIGHT) as usize);
    }

    #[test]
    fn win() {
        let mut session = create_session();
        for (y, x) in MINES.iter().skip(1) {
            let index = Index::from_coordinates_yx(&session, *y, *x);
            let (update, finished) = mark_valid(&mut session, index, Covery::Marked);
            assert!(finished.is_none());
            assert_eq!(update.iter().count(), 1);
            match update.iter().nth(0).unwrap().1 {
                CellInfo::MarkedEitherCorrectlyOrIncorrectly => {}
                _ => panic!("Not marked?"),
            }
        }

        let (update, finished) = {
            let (y, x) = MINES.iter().nth(0).unwrap();
            let index = Index::from_coordinates_yx(&session, *y, *x);
            mark_valid(&mut session, index, Covery::Marked)
        };

        // finished
        let finished = match finished {
            Some(finished) => finished,
            None => panic!("Not finished"),
        };
        assert!(finished.player_that_did_boom.is_none());
        let stats = &finished.stats[&DEFAULT_PLAYER_ID];
        assert_eq!(stats.mines_marked_correctly, MINES.iter().count() as u32);
        assert_eq!(stats.mines_marked_incorrectly, 0);
        assert_eq!(stats.cleared_correct_markings, 0);
        assert_eq!(stats.cleared_incorrect_markings, 0);
        assert_eq!(stats.actions, MINES.iter().count() as u32);
        assert_eq!(stats.questioned, 0);
        assert_eq!(stats.cleared_questioned, 0);

        // update
        assert_eq!(update.iter().count(), (WIDTH * HEIGHT) as usize);
        for (y, x) in &MINES {
            let index = Index::from_coordinates_yx(&session, *y, *x);
            find_and_do(&update, index, |info| {
                assert_eq!(*info, CellInfo::MarkedCorrectly)
            });
        }
        for (y, x) in iter_not_mines() {
            let index = Index::from_coordinates_yx(&session, y, x);
            find_and_do(&update, index, |info| match info {
                CellInfo::NeighboringMines(_) => {}
                _ => panic!("This is not an empty space"),
            });
        }
    }

    #[test]
    fn stuff() {
        let mut session = create_session();
        //mark correctly
        {
            let (y, x) = MINES.iter().nth(1).unwrap();
            let index = Index::from_coordinates_yx(&session, *y, *x);
            let (update, finished) = mark_valid(&mut session, index, Covery::Marked);
            assert!(finished.is_none());
            assert_eq!(update.iter().count(), 1);
            match update.iter().nth(0).unwrap().1 {
                CellInfo::MarkedEitherCorrectlyOrIncorrectly => {}
                _ => panic!("Not marked?"),
            }

            let stats = session.stats().nth(0).unwrap().1;
            assert_eq!(stats.mines_marked_correctly, 1);
            assert_eq!(stats.mines_marked_incorrectly, 0);
            assert_eq!(stats.cleared_correct_markings, 0);
            assert_eq!(stats.cleared_incorrect_markings, 0);
            assert_eq!(stats.actions, 1);
            assert_eq!(stats.questioned, 0);
            assert_eq!(stats.cleared_questioned, 0);

            let stats = get_common_data_stats(&session);
            assert_eq!(stats.mines_marked_correctly, 1);
            assert_eq!(stats.mines_marked_incorrectly, 0);
        }
        //mark incorrectly
        for _ in 0..1 {
            let (y, x) = iter_not_mines().nth(1).unwrap();
            let index = Index::from_coordinates_yx(&session, y, x);
            let (update, finished) = mark_valid(&mut session, index, Covery::Marked);
            assert!(finished.is_none());
            assert_eq!(update.iter().count(), 1);
            match update.iter().nth(0).unwrap().1 {
                CellInfo::MarkedEitherCorrectlyOrIncorrectly => {}
                _ => panic!("Not marked?"),
            }

            let stats = session.stats().nth(0).unwrap().1;
            assert_eq!(stats.mines_marked_correctly, 1);
            assert_eq!(stats.mines_marked_incorrectly, 1);
            assert_eq!(stats.cleared_correct_markings, 0);
            assert_eq!(stats.cleared_incorrect_markings, 0);
            assert_eq!(stats.actions, 2);
            assert_eq!(stats.questioned, 0);
            assert_eq!(stats.cleared_questioned, 0);

            let stats = get_common_data_stats(&session);
            assert_eq!(stats.mines_marked_correctly, 1);
            assert_eq!(stats.mines_marked_incorrectly, 1);
        }
        //clear incorrect mark, question instead
        {
            let (y, x) = iter_not_mines().nth(1).unwrap();
            let index = Index::from_coordinates_yx(&session, y, x);
            let (update, finished) = mark_valid(&mut session, index, Covery::Questioned);
            assert!(finished.is_none());
            assert_eq!(update.iter().count(), 1);
            match update.iter().nth(0).unwrap().1 {
                CellInfo::Questioned => {}
                _ => panic!("Not marked?"),
            }

            let stats = session.stats().nth(0).unwrap().1;
            assert_eq!(stats.mines_marked_correctly, 1);
            assert_eq!(stats.mines_marked_incorrectly, 1);
            assert_eq!(stats.cleared_correct_markings, 0);
            assert_eq!(stats.cleared_incorrect_markings, 1);
            assert_eq!(stats.actions, 3);
            assert_eq!(stats.questioned, 1);
            assert_eq!(stats.cleared_questioned, 0);

            let stats = get_common_data_stats(&session);
            assert_eq!(stats.mines_marked_correctly, 1);
            assert_eq!(stats.mines_marked_incorrectly, 0);
        }
        //clear correct mark
        {
            let (y, x) = MINES.iter().nth(1).unwrap();
            let index = Index::from_coordinates_yx(&session, *y, *x);
            let (update, finished) = mark_valid(&mut session, index, Covery::Covered);
            assert!(finished.is_none());
            assert_eq!(update.iter().count(), 1);
            match update.iter().nth(0).unwrap().1 {
                CellInfo::Covered => {}
                _ => panic!("Not covered?"),
            }

            let stats = session.stats().nth(0).unwrap().1;
            assert_eq!(stats.mines_marked_correctly, 1);
            assert_eq!(stats.mines_marked_incorrectly, 1);
            assert_eq!(stats.cleared_correct_markings, 1);
            assert_eq!(stats.cleared_incorrect_markings, 1);
            assert_eq!(stats.actions, 4);
            assert_eq!(stats.questioned, 1);
            assert_eq!(stats.cleared_questioned, 0);

            let stats = get_common_data_stats(&session);
            assert_eq!(stats.mines_marked_correctly, 0);
            assert_eq!(stats.mines_marked_incorrectly, 0);
        }
        //clear question
        {
            let (y, x) = iter_not_mines().nth(1).unwrap();
            let index = Index::from_coordinates_yx(&session, y, x);
            let (update, finished) = mark_valid(&mut session, index, Covery::Covered);
            assert!(finished.is_none());
            assert_eq!(update.iter().count(), 1);
            match update.iter().nth(0).unwrap().1 {
                CellInfo::Covered => {}
                _ => panic!("Not covered?"),
            }

            let stats = session.stats().nth(0).unwrap().1;
            assert_eq!(stats.mines_marked_correctly, 1);
            assert_eq!(stats.mines_marked_incorrectly, 1);
            assert_eq!(stats.cleared_correct_markings, 1);
            assert_eq!(stats.cleared_incorrect_markings, 1);
            assert_eq!(stats.actions, 5);
            assert_eq!(stats.questioned, 1);
            assert_eq!(stats.cleared_questioned, 1);

            let stats = get_common_data_stats(&session);
            assert_eq!(stats.mines_marked_correctly, 0);
            assert_eq!(stats.mines_marked_incorrectly, 0);
        }
    }
}
