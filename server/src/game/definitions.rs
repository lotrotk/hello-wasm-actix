use super::*;

#[must_use]
#[derive(Clone, Copy, Debug)]
pub enum PlayersNeedStatusUpdate {
    Yes,
    No,
}

pub trait Player: Send + 'static {
    type SenderFromServerToClient: super::SenderFromServerToClient;

    fn new(id: PlayerID, channel_to_client: Self::SenderFromServerToClient) -> Self;

    fn on_quit_game<P: PlayerManager<Item = Self>, SM: SessionManager, PM: PlayerInfoManager>(
        player: PlayerID,
        players: &mut P,
        sessions: &mut SM,
        players_info: &PM,
        session: GameID,
    ) -> PlayersNeedStatusUpdate;

    fn on_disconnected<P: PlayerManager<Item = Self>, SM: SessionManager, PM: PlayerInfoManager>(
        player: PlayerID,
        players: &mut P,
        sessions: &mut SM,
        players_info: &PM,
    ) -> PlayersNeedStatusUpdate;

    fn take_action_on_message<PM: PlayerInfoManager, SM: SessionManager>(
        &mut self,
        players: &mut PM,
        sessions: &mut SM,
        message: from_client::GameMessage,
    ) -> Result<PlayersNeedStatusUpdate, &'static str>;

    fn participate<P: PlayerManager<Item = Self>, SM: SessionManager, PM: PlayerInfoManager>(
        player: PlayerID,
        session: GameID,
        players: &mut P,
        sessions: &mut SM,
        players_info: &PM,
        title: &str,
    );

    fn rename(&mut self, name: RenamePlayer);

    fn send_msg(&mut self, msg: messages::from_server::Message);
}

pub trait PlayerInfoManager {
    fn insert(&mut self, player: PlayerID);
    fn remove(&mut self, player: PlayerID);
    fn name(&self, player: PlayerID) -> &str;
    fn rename(&mut self, player: PlayerID, new_name: String) -> RenamePlayer;
}

pub trait SessionManager {
    fn find(&mut self, id: GameID) -> Option<&mut session::Session>;
    fn add_new_session_and_add_player_to_it(
        &mut self,
        new_session_title: String,
        new_session: session::Game,
        player: PlayerID,
    ) -> Result<GameID, &'static str>;
    fn remove(&mut self, session: GameID);
}

pub trait PlayerManager {
    type Item: Player;
    fn get_mut(&mut self, index: PlayerID) -> &mut Self::Item;
}

pub enum RenamePlayer {
    ValidName(String),
    InvalidName { name: String, reason: String },
}

#[cfg(test)]
pub mod tests {
    use super::super::player::tests::PlayerMocked;
    use super::*;

    use mockall::{automock, mock};

    #[automock]
    pub trait MyPlayerInfoManager {
        fn insert(&mut self, player: PlayerID);
        fn remove(&mut self, player: PlayerID);
        fn name(&self, player: PlayerID) -> &str;
        fn rename(&mut self, player: PlayerID, new_name: String) -> RenamePlayer;
    }

    impl PlayerInfoManager for MockMyPlayerInfoManager {
        fn insert(&mut self, player: PlayerID) {
            MyPlayerInfoManager::insert(self, player)
        }
        fn remove(&mut self, player: PlayerID) {
            MyPlayerInfoManager::remove(self, player)
        }
        fn name(&self, player: PlayerID) -> &str {
            MyPlayerInfoManager::name(self, player)
        }
        fn rename(&mut self, player: PlayerID, new_name: String) -> RenamePlayer {
            MyPlayerInfoManager::rename(self, player, new_name)
        }
    }

    mock! {
        pub MyPlayerManager {}

        impl PlayerManager for MyPlayerManager {
            type Item = PlayerMocked;
            fn get_mut(&mut self, index: PlayerID) -> &mut <Self as PlayerManager>::Item;
        }
    }

    mod session_manager {
        use super::*;

        #[automock]
        pub trait MySessionManager {
            fn find(&mut self, id: GameID);
            fn add_new_session_and_add_player_to_it(
                &mut self,
                new_session_title: String,
                new_session: session::Game,
                player: PlayerID,
            ) -> Result<GameID, &'static str>;
            fn remove(&mut self, session: GameID);
        }
    }

    pub struct MockMySessionManager {
        pub session: Option<session::Session>,
        pub mock: session_manager::MockMySessionManager,
    }

    impl MockMySessionManager {
        pub fn new() -> Self {
            Self {
                session: None,
                mock: session_manager::MockMySessionManager::new(),
            }
        }
    }

    impl SessionManager for MockMySessionManager {
        fn find(&mut self, id: GameID) -> Option<&mut session::Session> {
            self.mock.find(id);
            use self::session_manager::MySessionManager;
            self.session.as_mut()
        }
        fn add_new_session_and_add_player_to_it(
            &mut self,
            new_session_title: String,
            new_session: session::Game,
            player: PlayerID,
        ) -> Result<GameID, &'static str> {
            use self::session_manager::MySessionManager;
            self.mock
                .add_new_session_and_add_player_to_it(new_session_title, new_session, player)
        }
        fn remove(&mut self, session: GameID) {
            use self::session_manager::MySessionManager;
            self.mock.remove(session)
        }
    }
}
