use super::*;

pub struct PlayerPoolFront<F: ServerToClientChannelFactory> {
    players: BTreeSet<PlayerID>,
    max_players: u32,
    sender_from_client_to_server_blueprint:
        SenderFromClientToServer<<F as ServerToClientChannelFactory>::Sender>,
    server_to_client_channel_factory: F,
}

impl<F: ServerToClientChannelFactory> PlayerPoolFront<F> {
    pub fn new(
        max_players: u32,
        sender_from_client_to_server_blueprint: SenderFromClientToServer<
            <F as ServerToClientChannelFactory>::Sender,
        >,
        server_to_client_channel_factory: F,
    ) -> Self {
        let players = BTreeSet::new();
        Self {
            players,
            max_players,
            sender_from_client_to_server_blueprint,
            server_to_client_channel_factory,
        }
    }

    pub fn next_available_id(&self) -> Option<PlayerID> {
        if self.players.len() >= self.max_players as usize {
            return None;
        }

        impl From<u32> for PlayerID {
            fn from(number: u32) -> Self {
                PlayerID(number)
            }
        }
        impl From<PlayerID> for u32 {
            fn from(id: PlayerID) -> Self {
                let PlayerID(number) = id;
                number
            }
        }

        let id = match NextAvailableIdSearchUsage::find(self.players.iter().copied()) {
            NextAvailableIdSearchUsage::None => PlayerID(0),
            NextAvailableIdSearchUsage::CanBe(id) => id,
            NextAvailableIdSearchUsage::MayBe(id) => id,
        };
        Some(id)
    }

    pub fn remove_player(&mut self, id: PlayerID) {
        let remove_player_msg = InternalPoolMessageContent::StoppedPlayer(id).into();
        self.sender_from_client_to_server_blueprint
            .send(remove_player_msg)
            .expect("Failed to send internal message (remove player)");
        let _held_player = self.players.remove(&id);
    }
}

impl<F: ServerToClientChannelFactory> crate::game::PlayerPoolFront for PlayerPoolFront<F> {
    type Sender = <F as ServerToClientChannelFactory>::Sender;
    type Receiver = <F as ServerToClientChannelFactory>::Receiver;

    fn create_player(&mut self) -> Option<(Self::Receiver, PlayerToServer<Self::Sender>)> {
        let id = match self.next_available_id() {
            Some(id) => id,
            None => return None,
        };
        let is_new_player = self.players.insert(id);
        assert!(is_new_player, "player already exists");

        let (sender_from_server_to_client, receiver_from_server_to_client) =
            self.server_to_client_channel_factory.channel();

        let new_player_msg = InternalPoolMessageContent::NewPlayer {
            id,
            sender_from_server_to_client,
        }
        .into();
        self.sender_from_client_to_server_blueprint
            .send(new_player_msg)
            .expect("Failed to send internal message (new player)");
        println!("Created new {:?}", id);

        let player_to_server = PlayerToServer {
            id,
            sender_from_client_to_server: self.sender_from_client_to_server_blueprint.clone(),
        };
        Some((receiver_from_server_to_client, player_to_server))
    }
}
