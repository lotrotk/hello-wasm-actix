use super::*;

pub enum MessageToServer<S: SenderFromServerToClient> {
    Public(from_client::Message),
    Private(InternalPoolMessage<S>),
}

pub struct InternalPoolMessage<S: SenderFromServerToClient> {
    pub content: InternalPoolMessageContent<S>,
}

pub enum InternalPoolMessageContent<S: SenderFromServerToClient> {
    NewPlayer {
        id: PlayerID,
        sender_from_server_to_client: S,
    },
    StoppedPlayer(PlayerID),
}

impl<S: SenderFromServerToClient> From<from_client::Message> for MessageToServer<S> {
    fn from(msg: from_client::Message) -> Self {
        Self::Public(msg)
    }
}

impl<S: SenderFromServerToClient> From<InternalPoolMessageContent<S>> for MessageToServer<S> {
    fn from(content: InternalPoolMessageContent<S>) -> Self {
        let msg = InternalPoolMessage { content };
        Self::Private(msg)
    }
}
