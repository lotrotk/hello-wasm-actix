mod back;
mod front;
mod internal_messages;
mod next_available_search;
mod player_info_manager;
mod session_manager;
mod thread;

pub use internal_messages::MessageToServer;

use std::collections::{BTreeMap, BTreeSet};
use std::sync::{atomic, mpsc, Arc, Mutex};

use super::*;

use back::PlayerPoolBack;
use front::PlayerPoolFront;
use internal_messages::*;
use next_available_search::NextAvailableIdSearchUsage;
use player_info_manager::PlayerInfoManager;
use session_manager::SessionManager;
use thread::PlayerPoolThread;

pub fn create_pool<P, F>(
    server_name: String,
    max_players: u32,
    max_sessions: u32,
    server_to_client_channel_factory: F,
) -> (Arc<Mutex<PlayerPoolFront<F>>>, PlayerPoolThread)
where
    P: definitions::Player<SenderFromServerToClient = <F as ServerToClientChannelFactory>::Sender>,
    F: ServerToClientChannelFactory,
{
    let (sender_from_client_to_server, receiver_from_client_to_server) = mpsc::channel();
    let front = Arc::new(Mutex::new(PlayerPoolFront::new(
        max_players,
        sender_from_client_to_server,
        server_to_client_channel_factory,
    )));
    let back = PlayerPoolBack::<P>::new(server_name, max_sessions);
    let thread = PlayerPoolThread::new::<P, F>(receiver_from_client_to_server, front.clone(), back);
    (front, thread)
}
