pub enum NextAvailableIdSearchUsage<ID: Copy + Into<u32> + From<u32>> {
    None,
    CanBe(ID),
    MayBe(ID),
}

impl<ID: Copy + Into<u32> + From<u32>> NextAvailableIdSearchUsage<ID> {
    pub fn find<IT: std::iter::Iterator<Item = ID>>(mut used_ids: IT) -> Self {
        let mut a = match used_ids.next() {
            Some(first) => first,
            None => {
                return Self::None;
            }
        };
        for b in used_ids {
            let aid: u32 = a.into();
            let bid: u32 = b.into();
            if bid != aid + 1 {
                return Self::CanBe((aid + 1).into());
            }
            a = b;
        }
        let aid: u32 = a.into();
        Self::MayBe((aid + 1).into())
    }
}
