use super::*;

pub struct PlayerPoolBack<P>
where
    P: definitions::Player,
{
    server_name: String,
    players: BTreeMap<PlayerID, P>,
    player_info_manager: PlayerInfoManager,
    session_manager: SessionManager,
}

impl<P> PlayerPoolBack<P>
where
    P: definitions::Player,
{
    pub fn new(server_name: String, max_sessions: u32) -> Self {
        Self {
            player_info_manager: PlayerInfoManager::default(),
            players: BTreeMap::new(),
            server_name,
            session_manager: SessionManager::new(max_sessions),
        }
    }

    fn create_status_message(&self) -> from_server::ServerStatus {
        let sessions = self
            .session_manager
            .sessions()
            .map(|(id, session)| {
                let descr = from_server::SessionDescription {
                    game_type: session.get_type(),
                    title: session.get_title().clone(),
                };
                (id, descr)
            })
            .collect();

        from_server::ServerStatus {
            name: self.server_name.clone(),
            sessions,
        }
    }

    pub fn insert_new_player(
        &mut self,
        id: PlayerID,
        mut sender_from_server_to_client: P::SenderFromServerToClient,
    ) {
        sender_from_server_to_client
            .send(from_server::Message::Status(self.create_status_message()));
        sender_from_server_to_client.send(from_server::Message::PlayerRegistrationReply(
            from_server::PlayerRegistrationReply::RegisteredWithoutName,
        ));
        let player = P::new(id, sender_from_server_to_client);
        let previous_player = self.players.insert(id, player);
        assert!(previous_player.is_none(), "player already exists");

        use definitions::PlayerInfoManager;
        self.player_info_manager.insert(id);
    }

    pub fn quit_game_for_player(&mut self, player: PlayerID, session: GameID) {
        let mut players = MyPlayers(&mut self.players);
        let need_update = P::on_quit_game(
            player,
            &mut players,
            &mut self.session_manager,
            &self.player_info_manager,
            session,
        );
        self.send_update_to_players(need_update)
    }

    pub fn remove_player(&mut self, player: PlayerID) {
        if !self.players.contains_key(&player) {
            return;
        }
        let mut players = MyPlayers(&mut self.players);
        let need_update = P::on_disconnected(
            player,
            &mut players,
            &mut self.session_manager,
            &self.player_info_manager,
        );
        self.players.remove(&player);
        use definitions::PlayerInfoManager;
        self.player_info_manager.remove(player);
        self.send_update_to_players(need_update)
    }

    fn send_update_to_players(&mut self, need_update: definitions::PlayersNeedStatusUpdate) {
        if let definitions::PlayersNeedStatusUpdate::Yes = need_update {
            let update = self.create_status_message();
            for player in self.players.values_mut() {
                player.send_msg(from_server::Message::Status(update.clone()));
            }
        }
    }

    pub fn pass_game_message(&mut self, player: PlayerID, message: from_client::GameMessage) {
        if let Some(player) = self.players.get_mut(&player) {
            let should_update_all_players = player.take_action_on_message(
                &mut self.player_info_manager,
                &mut self.session_manager,
                message,
            );
            if let Ok(need_update) = should_update_all_players {
                self.send_update_to_players(need_update)
            }
        }
    }

    pub fn rename_player(&mut self, id: PlayerID, name: String) {
        if let Some(player) = self.players.get_mut(&id) {
            use definitions::PlayerInfoManager;
            let renaming = self.player_info_manager.rename(id, name);
            player.rename(renaming);
        }
    }

    pub fn participate(&mut self, id: PlayerID, session: GameID, title: String) {
        if !self.players.contains_key(&id) {
            return;
        }
        let mut players = MyPlayers(&mut self.players);
        P::participate(
            id,
            session,
            &mut players,
            &mut self.session_manager,
            &self.player_info_manager,
            &title,
        )
    }
}

struct MyPlayers<'a, P: definitions::Player>(&'a mut BTreeMap<PlayerID, P>);

impl<'a, P: definitions::Player> definitions::PlayerManager for MyPlayers<'a, P> {
    type Item = P;

    fn get_mut(&mut self, index: PlayerID) -> &mut Self::Item {
        self.0.get_mut(&index).unwrap()
    }
}
