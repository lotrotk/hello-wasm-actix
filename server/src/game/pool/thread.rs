use super::*;

pub struct PlayerPoolThread {
    running: Arc<atomic::AtomicBool>,
    thread: Option<std::thread::JoinHandle<()>>,
}

impl PlayerPoolThread {
    pub fn new<P, F>(
        receiver_from_client_to_server: mpsc::Receiver<
            MessageToServer<<F as ServerToClientChannelFactory>::Sender>,
        >,
        front: Arc<Mutex<PlayerPoolFront<F>>>,
        back: PlayerPoolBack<P>,
    ) -> Self
    where
        P: definitions::Player<
            SenderFromServerToClient = <F as ServerToClientChannelFactory>::Sender,
        >,
        F: ServerToClientChannelFactory,
    {
        let running = Arc::new(atomic::AtomicBool::new(true));
        let thread = {
            let running = running.clone();
            std::thread::spawn(move || {
                player_pool_thread_fn(&running, receiver_from_client_to_server, front, back)
            })
        };
        Self {
            running,
            thread: Some(thread),
        }
    }
}

impl Drop for PlayerPoolThread {
    fn drop(&mut self) {
        self.running.store(false, atomic::Ordering::Release);
        if let Some(thread) = self.thread.take() {
            thread.join().unwrap()
        }
    }
}

impl crate::game::PlayerPoolThread for PlayerPoolThread {}

fn player_pool_thread_fn<P, F>(
    running: &atomic::AtomicBool,
    receiver_from_client_to_server: mpsc::Receiver<
        MessageToServer<<F as ServerToClientChannelFactory>::Sender>,
    >,
    front: Arc<Mutex<PlayerPoolFront<F>>>,
    mut back: PlayerPoolBack<P>,
) where
    P: definitions::Player<SenderFromServerToClient = <F as ServerToClientChannelFactory>::Sender>,
    F: ServerToClientChannelFactory,
{
    while running.load(atomic::Ordering::Acquire) {
        if let Ok(msg) = receiver_from_client_to_server.recv_timeout(MESG_RECV_TIMOUT) {
            handle_any_message(msg, &front, &mut back);
        }
    }
}

const MESG_RECV_TIMOUT: std::time::Duration = std::time::Duration::from_secs(1);

fn handle_any_message<P, F>(
    message: MessageToServer<<F as ServerToClientChannelFactory>::Sender>,
    front: &Mutex<PlayerPoolFront<F>>,
    back: &mut PlayerPoolBack<P>,
) where
    P: definitions::Player<SenderFromServerToClient = <F as ServerToClientChannelFactory>::Sender>,
    F: ServerToClientChannelFactory,
{
    match message {
        MessageToServer::Private(message) => {
            let InternalPoolMessage { content } = message;
            match content {
                InternalPoolMessageContent::NewPlayer {
                    id,
                    sender_from_server_to_client,
                } => back.insert_new_player(id, sender_from_server_to_client),
                InternalPoolMessageContent::StoppedPlayer(id) => back.remove_player(id),
            }
        }
        MessageToServer::Public(message) => {
            let from_client::Message(player, content) = message;
            match content {
                from_client::Content::QuitGame(game) => back.quit_game_for_player(player, game),
                from_client::Content::Disconnected => {
                    let front = &mut front.lock().unwrap();
                    back.remove_player(player);
                    front.remove_player(player);
                }
                from_client::Content::PlayerRegistration { name } => {
                    back.rename_player(player, name)
                }
                from_client::Content::Participate { session, title } => {
                    back.participate(player, session, title)
                }
                from_client::Content::GameMessage(msg) => back.pass_game_message(player, msg),
            }
        }
    }
}
