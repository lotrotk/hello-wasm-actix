use super::*;

pub struct SessionManager {
    sessions: BTreeMap<GameID, session::Session>,
    max_sessions: u32,
}

impl SessionManager {
    pub fn new(max_sessions: u32) -> Self {
        let sessions = BTreeMap::new();
        Self {
            sessions,
            max_sessions,
        }
    }

    pub fn sessions(&self) -> impl Iterator<Item = (GameID, &session::Session)> {
        self.sessions.iter().map(|(id, session)| (*id, session))
    }
}

impl definitions::SessionManager for SessionManager {
    fn find(&mut self, id: GameID) -> Option<&mut session::Session> {
        self.sessions.get_mut(&id)
    }

    fn add_new_session_and_add_player_to_it(
        &mut self,
        new_session_title: String,
        new_session: session::Game,
        player: PlayerID,
    ) -> Result<GameID, &'static str> {
        let (new_session, session) = {
            impl From<u32> for GameID {
                fn from(number: u32) -> Self {
                    GameID(number)
                }
            }
            impl From<GameID> for u32 {
                fn from(id: GameID) -> Self {
                    let GameID(number) = id;
                    number
                }
            }

            let id = match NextAvailableIdSearchUsage::find(self.sessions.keys().copied()) {
                NextAvailableIdSearchUsage::None => GameID(0),
                NextAvailableIdSearchUsage::CanBe(id) => id,
                NextAvailableIdSearchUsage::MayBe(id) => id,
            };

            let mut session = {
                let game = new_session;
                let title = new_session_title;
                session::Session::new(id, title, game)
            };
            session.add_participater(player);
            (id, session)
        };

        if self.sessions.len() >= self.max_sessions as usize {
            return Err("Too many active sessions going on");
        }

        let previous_session = self.sessions.insert(new_session, session);
        assert!(previous_session.is_none(), "Session already exists");
        Ok(new_session)
    }

    fn remove(&mut self, session: GameID) {
        self.sessions.remove(&session);
    }
}
