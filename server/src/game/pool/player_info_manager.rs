use super::*;

use std::collections::BTreeMap;

#[derive(Default)]
pub struct PlayerInfoManager {
    data: BTreeMap<PlayerID, PlayerInfo>,
}

impl definitions::PlayerInfoManager for PlayerInfoManager {
    fn insert(&mut self, player: PlayerID) {
        let previous = self.data.insert(player, PlayerInfo::default());
        assert!(previous.is_none());
    }

    fn remove(&mut self, player: PlayerID) {
        self.data.remove(&player);
    }

    fn name(&self, player: PlayerID) -> &str {
        self.data[&player].name()
    }

    fn rename(&mut self, player: PlayerID, new_name: String) -> definitions::RenamePlayer {
        if new_name.len() >= 2 {
            self.data.get_mut(&player).unwrap().rename(new_name.clone());
            definitions::RenamePlayer::ValidName(new_name)
        } else {
            definitions::RenamePlayer::InvalidName {
                name: new_name,
                reason: "this name is too short".into(),
            }
        }
    }
}

#[derive(Debug)]
enum PlayerInfo {
    Registered { name: String },
    Unregistered,
}

impl PlayerInfo {
    pub fn name(&self) -> &str {
        match self {
            Self::Registered { name } => name,
            Self::Unregistered => "UNREGISTERED PLAYER",
        }
    }

    pub fn rename(&mut self, new_name: String) {
        *self = Self::Registered { name: new_name };
    }
}

impl Default for PlayerInfo {
    fn default() -> Self {
        Self::Unregistered
    }
}
