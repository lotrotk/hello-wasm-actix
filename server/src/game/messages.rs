use super::*;

pub mod mines {
    use super::*;

    #[derive(Debug, Eq, PartialEq)]
    pub struct Coordinate {
        pub y: u32,
        pub x: u32,
    }

    #[derive(Clone, Debug, Eq, PartialEq)]
    pub struct NewGame {
        pub width: u32,
        pub height: u32,
        pub mines: u32,
        pub title: String,
    }

    pub mod from_client {
        use super::*;

        #[derive(Debug, Eq, PartialEq)]
        pub enum CellMarking {
            Clear,
            Mine,
            Question,
            Safe,
        }

        #[derive(Debug, Eq, PartialEq)]
        pub enum Message {
            Marking(GameID, Coordinate, CellMarking),
            RequestNewGame(NewGame),
        }
    }

    pub mod from_server {
        use super::*;

        #[derive(Debug, Eq, PartialEq)]
        pub enum Tile {
            Covered,
            EmptyWithNeighboringMines(u8),
            ExplodedMine,
            MarkingThatIsCorrect,
            MarkingThatIsIncorrect,
            MarkingThatMayBeCorrectOrIncorrect,
            Mine,
            Questioned,
        }

        #[derive(Debug, Eq, PartialEq)]
        pub enum Marking {
            UpdatedTiles(Vec<(Coordinate, Tile)>),
            Failure(String),
        }

        #[derive(Debug, Eq, PartialEq)]
        pub struct PlayerStats {
            pub player_name: String,
            pub mines_marked_correctly: u32,
            pub mines_marked_incorrectly: u32,
            pub cleared_correct_markings: u32,
            pub cleared_incorrect_markings: u32,
            pub actions: u32,
            pub questioned: u32,
            pub cleared_questioned: u32,
        }

        #[derive(Debug, Eq, PartialEq)]
        pub enum Message {
            ConfirmNewGame(NewGame),
            Lost { player_that_did_boom: String },
            Marking(Marking),
            PlayerStats(Vec<PlayerStats>),
            Score { player: String, score: Score },
            Won,
        }
    }
}

pub mod from_client {
    use super::*;

    #[derive(Debug, Eq, PartialEq)]
    pub enum GameMessage {
        Mines(mines::from_client::Message),
    }

    #[derive(Debug, Eq, PartialEq)]
    pub enum Content {
        Disconnected,
        GameMessage(GameMessage),
        Participate { session: GameID, title: String },
        PlayerRegistration { name: String },
        QuitGame(GameID),
    }

    #[derive(Debug, Eq, PartialEq)]
    pub struct Message(pub PlayerID, pub Content);
}

pub mod from_server {
    use super::*;

    #[derive(Clone, Debug, Eq, PartialEq)]
    pub struct Participation {
        pub session: GameID,
        pub player: String,
        pub player_is_you: bool,
    }

    #[derive(Debug, Eq, PartialEq)]
    pub enum PlayerRegistrationReply {
        Failure(String),
        RegisteredWithoutName,
        RegisteredWithName(String),
        RegisteredWithNameFailed { name: String, reason: String },
    }

    #[derive(Clone, Debug, Eq, PartialEq)]
    pub struct SessionDescription {
        pub game_type: GameType,
        pub title: String,
    }

    #[derive(Clone, Debug, Eq, PartialEq)]
    pub struct ServerStatus {
        pub name: String,
        pub sessions: Vec<(GameID, SessionDescription)>,
    }

    #[derive(Debug, Eq, PartialEq)]
    pub enum Message {
        Participation(Participation),
        PlayerRegistrationReply(PlayerRegistrationReply),
        Status(ServerStatus),
        Mines {
            session: GameID,
            game: mines::from_server::Message,
        },
        FailedToMakeGame {
            title: String,
            reason: String,
        },
        QuitGame {
            player: String,
            session: GameID,
            score: Score,
        },
    }
}
