mod websocket;

use actix_http::{http::StatusCode, ResponseBuilder};
use actix_web::{web, App, Error, HttpRequest, HttpResponse, HttpServer, Responder};

pub use websocket::{Message, ReceiverFromServerToClient, SenderFromClientToServer};

pub trait SocketPool<R, S>: Clone + Send
where
    R: ReceiverFromServerToClient,
    S: SenderFromClientToServer,
{
    fn on_new_connection(&self) -> Option<(R, S)>;
}

async fn index(text: &'static str) -> impl Responder {
    HttpResponse::Ok().body(text)
}

static WASM_INDEX_HTML: &str = include_str!("index.html");

async fn wasm_index() -> impl Responder {
    index(WASM_INDEX_HTML).await
}

async fn socket_server_index<P, R, S>(
    r: HttpRequest,
    stream: web::Payload,
    pool: P,
) -> Result<HttpResponse, Error>
where
    P: SocketPool<R, S>,
    R: ReceiverFromServerToClient,
    S: SenderFromClientToServer,
{
    if let Some((receiver_from_server_to_client, sender_from_client_to_server)) =
        pool.on_new_connection()
    {
        return websocket::websocket_index(
            r,
            stream,
            receiver_from_server_to_client,
            sender_from_client_to_server,
        )
        .await;
    }
    Err(ResponseBuilder::new(StatusCode::OK).into())
}

async fn main<P, R, S>(args: super::Arguments, pool: P) -> std::io::Result<()>
where
    P: SocketPool<R, S> + 'static,
    R: ReceiverFromServerToClient,
    S: SenderFromClientToServer,
{
    let host = format!("{}:{}", args.hostname, args.port);
    println!("Starting server at http://{}", host);
    HttpServer::new(move || {
        let pool = pool.clone();
        App::new()
            .route("/", web::get().to(wasm_index))
            .route(
                "/socketserver",
                web::get().to(move |r, s| socket_server_index(r, s, pool.clone())),
            )
            .service(actix_files::Files::new("/", &args.static_dir).show_files_listing())
    })
    .bind(host)?
    .run()
    .await
}

pub async fn main_tokio<P, R, S>(
    local: &tokio::task::LocalSet,
    args: super::Arguments,
    pool: P,
) -> std::io::Result<()>
where
    P: SocketPool<R, S> + 'static,
    R: ReceiverFromServerToClient,
    S: SenderFromClientToServer,
{
    let system = actix::System::run_in_tokio("The Actix System", local);
    let server_res = main(args, pool).await?;
    system.await?;
    #[allow(clippy::unit_arg)]
    Ok(server_res)
}
