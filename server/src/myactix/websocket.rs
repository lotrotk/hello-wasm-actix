use actix::prelude::{Actor, StreamHandler};
use actix_web::{web, Error, HttpRequest, HttpResponse};
use actix_web_actors::ws;

#[derive(Debug)]
pub enum Message {
    Text(String),
}

pub trait SenderFromClientToServer: Unpin + 'static {
    fn on_stopped(&self);
    fn on_message(&self, message: Message);
}

pub trait ReceiverFromServerToClient:
    futures::stream::Stream<Item = Message> + Unpin + 'static
{
}

enum MySocketState<R, S>
where
    S: SenderFromClientToServer,
    R: ReceiverFromServerToClient,
{
    NonActive {
        receiver_from_server_to_client: R,
        sender_from_client_to_server: S,
    },
    Active {
        sender_from_client_to_server: S,
    },
    Stopped,
}

impl<R, S> MySocketState<R, S>
where
    S: SenderFromClientToServer,
    R: ReceiverFromServerToClient,
{
    fn is_active(&self) -> bool {
        matches!(self, Self::Active { .. })
    }

    fn on_started(&mut self) -> Result<R, &'static str> {
        let mut current_state = Self::Stopped;
        std::mem::swap(self, &mut current_state);
        if let Self::NonActive {
            receiver_from_server_to_client,
            sender_from_client_to_server,
        } = current_state
        {
            let mut new_state = Self::Active {
                sender_from_client_to_server,
            };
            std::mem::swap(self, &mut new_state);
            Ok(receiver_from_server_to_client)
        } else {
            Err("can only start mysocketstate from state nonactive")
        }
    }

    fn on_stopped(&mut self) {
        match self {
            Self::NonActive {
                sender_from_client_to_server,
                ..
            } => sender_from_client_to_server.on_stopped(),
            Self::Active {
                sender_from_client_to_server,
            } => sender_from_client_to_server.on_stopped(),
            Self::Stopped => {}
        }
        *self = Self::Stopped;
    }
}

struct MySocket<R, S>(MySocketState<R, S>)
where
    S: SenderFromClientToServer,
    R: ReceiverFromServerToClient;

impl<R, S> MySocket<R, S>
where
    S: SenderFromClientToServer,
    R: ReceiverFromServerToClient,
{
    fn new(receiver_from_server_to_client: R, sender_from_client_to_server: S) -> Self {
        println!("Creating websocket");
        Self(MySocketState::NonActive {
            sender_from_client_to_server,
            receiver_from_server_to_client,
        })
    }
}

impl<R, S> Actor for MySocket<R, S>
where
    S: SenderFromClientToServer,
    R: ReceiverFromServerToClient,
{
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, context: &mut Self::Context) {
        println!("Websocket started");
        let Self(state) = self;
        let receiver_from_server_to_client = state.on_started().unwrap();
        Self::add_stream(receiver_from_server_to_client, context);
    }

    fn stopped(&mut self, _context: &mut Self::Context) {
        println!("Websocket stopped");
        let Self(state) = self;
        state.on_stopped();
    }
}

impl<R, S> StreamHandler<Message> for MySocket<R, S>
where
    S: SenderFromClientToServer,
    R: ReceiverFromServerToClient,
{
    fn handle(&mut self, item: Message, context: &mut Self::Context) {
        let Self(state) = self;
        if !state.is_active() {
            return;
        }
        let Message::Text(msg) = item;
        context.text(msg);
    }
}

impl<R, S> StreamHandler<Result<ws::Message, ws::ProtocolError>> for MySocket<R, S>
where
    S: SenderFromClientToServer,
    R: ReceiverFromServerToClient,
{
    fn handle(
        &mut self,
        item: Result<ws::Message, ws::ProtocolError>,
        _context: &mut Self::Context,
    ) {
        let Self(state) = self;
        let sender_from_client_to_server = match state {
            MySocketState::Active {
                sender_from_client_to_server,
            } => sender_from_client_to_server,
            _ => return,
        };
        let msg = match item {
            Ok(msg) => msg,
            Err(e) => {
                eprintln!("Websocket failed to receive message with error {}", e);
                return;
            }
        };
        let msg = match msg {
            ws::Message::Text(txt) => {
                let msg = Message::Text(txt);
                sender_from_client_to_server.on_message(msg);
                None
            }
            ws::Message::Close(reason) => {
                if let Some(reason) = reason {
                    println!("received close message from websocket, reason={:?}", reason);
                } else {
                    println!("received close message from websocket, reason unknown");
                }
                state.on_stopped();
                None
            }
            _ => Some(msg),
        };
        if let Some(msg) = &msg {
            eprintln!(
                "Websocket received message that is not interpreted: {:?}",
                msg
            );
        }
    }
}

pub async fn websocket_index<R, S>(
    r: HttpRequest,
    stream: web::Payload,
    receiver_from_server_to_client: R,
    sender_from_client_to_server: S,
) -> Result<HttpResponse, Error>
where
    R: ReceiverFromServerToClient,
    S: SenderFromClientToServer,
{
    ws::start(
        MySocket::new(receiver_from_server_to_client, sender_from_client_to_server),
        &r,
        stream,
    )
}
