#!/usr/bin/fish
rustup update; and cargo install wasm-bindgen-cli
pushd server; cargo clean; cargo update; cargo update -p wasm-bindgen; popd
pushd library; cargo clean; cargo update; popd
pushd wasms/mines; cargo clean; cargo update; cargo update -p wasm-bindgen; popd
