use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Serialize, Deserialize, Debug)]
#[cfg_attr(test, derive(Eq, PartialEq))]
pub struct Session(pub u32);
#[derive(Clone, Copy, Serialize, Deserialize, Debug)]
#[cfg_attr(test, derive(Eq, PartialEq))]
pub struct Score(pub i64);
#[derive(Clone, Copy, Serialize, Deserialize, Debug)]
#[cfg_attr(test, derive(Eq, PartialEq))]
pub struct Token(pub i64);

#[derive(Clone, Copy, Serialize, Deserialize, Debug)]
#[cfg_attr(test, derive(Eq, PartialEq))]
pub enum GameType {
    Mines = 1,
}

pub mod mines {
    use super::*;

    #[derive(Serialize, Deserialize, Debug)]
    #[cfg_attr(test, derive(Eq, PartialEq))]
    pub struct Coordinate {
        pub y: u32,
        pub x: u32,
    }

    #[derive(Serialize, Deserialize, Debug)]
    #[cfg_attr(test, derive(Eq, PartialEq))]
    pub struct NewGame {
        pub width: u32,
        pub height: u32,
        pub mines: u32,
        pub title: String,
    }

    pub mod from_client {
        use super::*;

        #[derive(Serialize, Deserialize, Debug)]
        #[cfg_attr(test, derive(Eq, PartialEq))]
        pub enum CellMarking {
            Clear,
            Mine,
            Question,
            Safe,
        }

        #[derive(Serialize, Deserialize, Debug)]
        #[cfg_attr(test, derive(Eq, PartialEq))]
        pub enum Message {
            Marking(Session, Coordinate, CellMarking),
            RequestNewGame(NewGame),
        }
    }

    pub mod from_server {
        use super::*;

        #[derive(Serialize, Deserialize, Debug)]
        #[cfg_attr(test, derive(Eq, PartialEq))]
        pub enum Tile {
            Covered,
            EmptyWithNeighboringMines(u8),
            ExplodedMine,
            MarkingThatIsCorrect,
            MarkingThatIsIncorrect,
            MarkingThatMayBeCorrectOrIncorrect,
            Mine,
            Questioned,
        }

        #[derive(Serialize, Deserialize, Debug)]
        #[cfg_attr(test, derive(Eq, PartialEq))]
        pub enum Marking {
            UpdatedTiles(Vec<(Coordinate, Tile)>),
            Failure(String),
        }

        #[derive(Serialize, Deserialize, Debug)]
        #[cfg_attr(test, derive(Eq, PartialEq))]
        pub struct PlayerStats {
            pub player_name: String,
            pub mines_marked_correctly: u32,
            pub mines_marked_incorrectly: u32,
            pub cleared_correct_markings: u32,
            pub cleared_incorrect_markings: u32,
            pub actions: u32,
            pub questioned: u32,
            pub cleared_questioned: u32,
        }

        #[derive(Serialize, Deserialize, Debug)]
        #[cfg_attr(test, derive(Eq, PartialEq))]
        pub enum Message {
            ConfirmNewGame(NewGame),
            Lost { player_that_did_boom: String },
            Marking(Marking),
            PlayerStats(Vec<PlayerStats>),
            Score { player: String, score: Score },
            Won,
        }
    }
}

pub mod from_client {
    use super::*;

    #[derive(Serialize, Deserialize, Debug)]
    #[cfg_attr(test, derive(Eq, PartialEq))]
    pub enum GameMessage {
        Mines(mines::from_client::Message),
    }

    #[derive(Serialize, Deserialize, Debug)]
    #[cfg_attr(test, derive(Eq, PartialEq))]
    pub enum Message {
        Participate { session: Session, title: String },
        GameMessage(GameMessage),
        PlayerRegistration { name: String },
        QuitGame(Session),
    }
}

pub mod from_server {
    use super::*;

    #[derive(Serialize, Deserialize, Debug)]
    #[cfg_attr(test, derive(Eq, PartialEq))]
    pub enum PlayerRegistrationReply {
        Failure(String),
        RegisteredWithoutName,
        RegisteredWithName(String),
        RegisteredWithNameFailed { name: String, reason: String },
    }

    #[derive(Serialize, Deserialize, Debug)]
    #[cfg_attr(test, derive(Eq, PartialEq))]
    pub struct Participation {
        pub session: Session,
        pub player: String,
        pub player_is_you: bool,
    }

    #[derive(Serialize, Deserialize, Debug)]
    #[cfg_attr(test, derive(Eq, PartialEq))]
    pub struct SessionDescription {
        pub game_type: GameType,
        pub title: String,
    }

    #[derive(Serialize, Deserialize, Debug)]
    #[cfg_attr(test, derive(Eq, PartialEq))]
    pub struct ServerStatus {
        pub name: String,
        pub sessions: Vec<(Session, SessionDescription)>,
    }

    #[derive(Serialize, Deserialize, Debug)]
    #[cfg_attr(test, derive(Eq, PartialEq))]
    pub enum Message {
        FailedToMakeGame {
            title: String,
            reason: String,
        },
        Mines {
            session: Session,
            game: mines::from_server::Message,
        },
        Participation(Participation),
        PlayerRegistrationReply(PlayerRegistrationReply),
        Status(ServerStatus),
        QuitGame {
            player: String,
            session: Session,
            score: Score,
        },
    }
}

pub fn serialize<M: Serialize>(msg: &M) -> String
where
    M: Serialize,
{
    serde_json::to_string(msg).unwrap()
}

pub fn deserialize<'a, M>(msg: &'a str) -> Result<M, impl std::error::Error>
where
    M: Deserialize<'a>,
{
    serde_json::from_str::<'a, M>(msg)
}

pub fn serialize_msg_from_client(msg: &from_client::Message) -> String {
    self::serialize(msg)
}

pub fn serialize_msg_from_server(msg: &from_server::Message) -> String {
    self::serialize(msg)
}

pub fn deserialize_msg_from_client<'a>(
    msg: &'a str,
) -> Result<from_client::Message, impl std::error::Error> {
    self::deserialize(msg)
}

pub fn deserialize_msg_from_server<'a>(
    msg: &'a str,
) -> Result<from_server::Message, impl std::error::Error> {
    self::deserialize(msg)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn serialize_and_deserialize<M>(msg: &M)
    where
        M: Serialize + for<'a> Deserialize<'a> + std::fmt::Debug + PartialEq,
    {
        let s = serialize(msg);
        let result = deserialize(&s);
        assert!(result.is_ok());
        let result: M = result.unwrap();
        assert_eq!(&result, msg);
    }

    #[test]
    fn serialize_and_deserialize_game_creation() {
        serialize_and_deserialize(&mines::NewGame {
            width: 25,
            height: 36,
            mines: 14,
            title: "my super awesome game!".to_string(),
        });
    }
}
