pub mod messages;

pub fn hello_world() -> &'static str {
    "Hello from the common library!"
}
