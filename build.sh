#!/bin/bash
WASM_DIR="$1"
STATIC_DIR="$WASM_DIR/web_build"
mkdir -p $STATIC_DIR

pushd server
cargo build
popd
pushd $WASM_DIR
cargo build --target wasm32-unknown-unknown
wasm-bindgen --out-dir web_build --target web --no-typescript target/wasm32-unknown-unknown/debug/*.wasm
popd
RUST_BACKTRACE=1 RUST_BACKTRACE=full server/target/debug/hello-wasm-actix --static-dir $STATIC_DIR
