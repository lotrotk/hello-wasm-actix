#!/bin/bash
pushd library
cargo update
cargo fix
cargo fmt
popd
pushd server
cargo update
cargo fix
cargo fmt
cargo build
popd
pushd $WASM_DIR
cargo update
cargo fix
cargo fmt
popd
