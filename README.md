Minimal proof of concept of having a setup where

* a server provides a html page with web assembly content
* the wasm is able to alter the document content and open a websocket to the server
* the server and wasm are able to exchange messages
* not strictly needed in this example, but also provided a library that can be used by both server and wasm

This code is programmed in Rust and based on the following pages

* https://github.com/actix/examples/blob/master/websocket/src/main.rs
* https://rustwasm.github.io/docs/wasm-bindgen/examples/websockets.html
* inspired by a blog on "This week in Rust" : https://ianjk.com/rust-gamejam

To build and run, simply do
```bash
./build.sh wasms/hello_world
```
