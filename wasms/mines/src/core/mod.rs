pub mod socket;

use hello_wasm_actix_lib::messages;

use std::collections::HashMap;

const MINES_FIXED_WIDTH: u32 = 20;
const MINES_FIXED_HEIGHT: u32 = 20;
const MINES_FIXED_MINES: u32 = 50;

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub struct Session(pub u32);
pub struct SessionDescription {
    pub id: Session,
    pub title: String,
    pub game_type: String,
    pub participating: bool,
}

impl From<messages::Session> for Session {
    fn from(s: messages::Session) -> Self {
        let messages::Session(s) = s;
        Self(s)
    }
}

impl From<Session> for messages::Session {
    fn from(s: Session) -> Self {
        let Session(s) = s;
        Self(s)
    }
}

struct SessionDescriptionCache {
    id: Session,
    game_type: messages::GameType,
}

pub trait Logger {
    fn info<M>(&self, message: M)
    where
        M: AsRef<str>;
    fn error<M>(&self, message: M)
    where
        M: AsRef<str>;
}

pub trait UserInterface {
    fn alert(&self, msg: &str);
    fn set_server_name(&mut self, name: String);
    fn set_sessions<I: Iterator<Item = SessionDescription>>(&mut self, it: I);
    fn rename(&mut self, new_name: &str);
    fn new_game_of_mines_participating(
        &mut self,
        title: String,
        session: Session,
        width: u32,
        height: u32,
        mines: u32,
    );
}

pub struct Core<L, UI, OS>
where
    L: Logger,
    UI: UserInterface,
    OS: socket::OutSocket,
{
    osocket: OS,
    logger: L,
    ui: UI,
    sessions: HashMap<String, SessionDescriptionCache>,
    participating: Option<Session>,
}

impl<L, UI, OS> Core<L, UI, OS>
where
    L: Logger,
    UI: UserInterface,
    OS: socket::OutSocket,
{
    pub fn new(out_socket: OS, ui: UI, logger: L) -> Self {
        Self {
            osocket: out_socket,
            logger,
            ui,
            sessions: HashMap::new(),
            participating: None,
        }
    }

    pub fn in_socket<'a>(&'a mut self) -> impl 'a + socket::InSocket {
        AsInSocket(self)
    }

    pub fn program_socket<F, E>(&self, f: F) -> E
    where
        F: FnOnce(&OS) -> E,
    {
        f(&self.osocket)
    }

    pub fn register_player(&mut self, name: String) {
        let msg = messages::from_client::Message::PlayerRegistration { name: name.clone() };
        match self
            .osocket
            .send_message(&messages::serialize_msg_from_client(&msg))
        {
            Ok(_) => {
                self.logger.info(format!("name {} sent!", name));
            }
            Err(e) => {
                self.logger.error(format!("Failed to send name: {:?}", e));
            }
        }
    }

    pub fn create_game_of_mines(&mut self, title: String) {
        let new_game = messages::mines::NewGame {
            width: MINES_FIXED_WIDTH,
            height: MINES_FIXED_HEIGHT,
            mines: MINES_FIXED_MINES,
            title: title.clone(),
        };
        let mines_msg = messages::mines::from_client::Message::RequestNewGame(new_game);
        let game_msg = messages::from_client::GameMessage::Mines(mines_msg);
        let msg = messages::from_client::Message::GameMessage(game_msg);
        match self
            .osocket
            .send_message(&messages::serialize_msg_from_client(&msg))
        {
            Ok(_) => {
                self.logger
                    .info(format!("new session request {} sent!", title));
            }
            Err(e) => {
                self.logger
                    .error(format!("Failed to request session: {:?}", e));
            }
        }
    }

    pub fn participate_game(&mut self, title: String) {
        let SessionDescriptionCache { id, game_type } = match self.sessions.get(&title) {
            Some(cache) => cache,
            None => {
                self.logger
                    .error(format!("No session found with named \"{}\"", title));
                return;
            }
        };
        let messages::GameType::Mines = game_type;
        let msg = messages::from_client::Message::Participate {
            session: id.clone().into(),
            title: title.clone(),
        };
        match self
            .osocket
            .send_message(&messages::serialize_msg_from_client(&msg))
        {
            Ok(_) => {
                self.logger
                    .info(format!("new session participation request {} sent!", title));
            }
            Err(e) => {
                self.logger
                    .error(format!("Failed to request session participation: {:?}", e));
            }
        }
    }

    fn update_ui_sessions(&mut self) {
        let participating = &self.participating;
        self.ui.set_sessions(
            self.sessions
                .iter()
                .map(|(title, cache)| SessionDescription {
                    id: cache.id,
                    title: title.clone(),
                    game_type: game_type_to_str(cache.game_type).into(),
                    participating: participating.map(|s| s == cache.id).unwrap_or(false),
                }),
        )
    }

    fn on_message(&mut self, msg: &str) {
        let msg = messages::deserialize_msg_from_server(msg)
            .expect("Received message has invalid format");
        match msg {
            messages::from_server::Message::Status(status) => self.on_status_message(status),
            messages::from_server::Message::PlayerRegistrationReply(registration) => {
                self.on_player_registration_reply(registration)
            }
            messages::from_server::Message::Participation(
                messages::from_server::Participation {
                    session,
                    player,
                    player_is_you,
                },
            ) => {
                if player_is_you {
                    self.participating = Some(session.into());
                    self.update_ui_sessions()
                }
            }
            messages::from_server::Message::Mines { session, game } => {
                self.on_mines_message(session, game)
            }
            _ => eprintln!("no action defined yet for this type of message"),
        }
    }

    fn on_status_message(&mut self, status: messages::from_server::ServerStatus) {
        let messages::from_server::ServerStatus { name, sessions } = status;
        self.ui.set_server_name(name);
        self.sessions = sessions
            .iter()
            .map(|(id, descr)| {
                let messages::from_server::SessionDescription { game_type, title } = descr;
                let cache = SessionDescriptionCache {
                    id: id.clone().into(),
                    game_type: *game_type,
                };
                (title.clone(), cache)
            })
            .collect();
        self.update_ui_sessions()
    }

    fn on_player_registration_reply(
        &mut self,
        registration: messages::from_server::PlayerRegistrationReply,
    ) {
        type Reply = messages::from_server::PlayerRegistrationReply;
        match registration {
            Reply::Failure(reason) => self
                .ui
                .alert(&format!("Failed to get a player id: {}", reason)),
            Reply::RegisteredWithoutName => self.ui.rename(""),
            Reply::RegisteredWithName(name) => self.ui.rename(&name),
            Reply::RegisteredWithNameFailed { name, reason } => self.ui.alert(&format!(
                "Failed to rename to \"{}\", reason: {}",
                name, reason
            )),
        }
    }

    fn on_mines_new_game_message(
        &mut self,
        session: messages::Session,
        new_game: messages::mines::NewGame,
    ) {
        let messages::mines::NewGame {
            width,
            height,
            mines,
            title,
        } = new_game;
        self.ui
            .new_game_of_mines_participating(title, session.into(), width, height, mines)
    }

    fn on_mines_message(
        &mut self,
        session: messages::Session,
        msg: messages::mines::from_server::Message,
    ) {
        type Message = messages::mines::from_server::Message;
        match msg {
            Message::ConfirmNewGame(new_game) => self.on_mines_new_game_message(session, new_game),
            Message::Lost { .. }
            | Message::Marking(_)
            | Message::PlayerStats(_)
            | Message::Score { .. }
            | Message::Won => {
                eprintln!("no action defined yet for this type of mines message")
            }
        }
    }
}

struct AsInSocket<'a, L, UI, OS>(&'a mut Core<L, UI, OS>)
where
    OS: socket::OutSocket,
    UI: UserInterface,
    L: Logger;

impl<'a, L, UI, OS> socket::InSocket for AsInSocket<'a, L, UI, OS>
where
    OS: socket::OutSocket,
    UI: UserInterface,
    L: Logger,
{
    fn on_error<E>(&mut self, error_message: E)
    where
        E: std::fmt::Debug,
    {
        self.0.logger.error(format!("event: {:?}", error_message));
    }

    fn on_open(&mut self) {
        self.0.logger.info("Websocket opened");
    }

    fn on_closed(&mut self) {
        self.0.logger.info("Websocket closed");
    }

    fn on_message<M>(&mut self, message: M)
    where
        M: AsRef<str>,
    {
        let msg = message.as_ref();
        self.0
            .logger
            .info(format!(r#"Received message from server: "{}""#, msg));
        self.0.on_message(msg.as_ref())
    }
}

fn game_type_to_str(game_type: messages::GameType) -> &'static str {
    let messages::GameType::Mines = game_type;
    return "mines";
}
