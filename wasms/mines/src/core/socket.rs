pub trait OutSocket {
    fn send_message(&mut self, message: &str) -> std::io::Result<()>;
}

pub trait InSocket {
    fn on_error<E>(&mut self, error_message: E)
    where
        E: std::fmt::Debug;

    fn on_open(&mut self);

    fn on_closed(&mut self);

    fn on_message<M>(&mut self, message: M)
    where
        M: AsRef<str>;
}
