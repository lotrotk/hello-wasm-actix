use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

pub fn clog(s: &str) {
    log(s)
}

#[macro_export]
macro_rules! console_log {
    ($($t:tt)*) => (crate::web::consolelog::clog(&format_args!($($t)*).to_string()))
}
