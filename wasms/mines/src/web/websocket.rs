use crate::core::socket;
use wasm_bindgen::prelude::*;

fn jsvalue_to_ioerror(error: JsValue) -> std::io::Error {
    let msg = error
        .as_string()
        .unwrap_or("UNABLE TO READ ERROR MESSAGE".to_string());
    std::io::Error::new(std::io::ErrorKind::Other, msg)
}

impl socket::OutSocket for web_sys::WebSocket {
    fn send_message(&mut self, message: &str) -> std::io::Result<()> {
        self.send_with_str(message).map_err(jsvalue_to_ioerror)
    }
}
