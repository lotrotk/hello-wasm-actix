#[macro_use]
pub mod consolelog;
mod core;
mod websocket;

pub use self::core::create_core;
pub use self::core::Core;
