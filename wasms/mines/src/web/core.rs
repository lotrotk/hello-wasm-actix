use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{ErrorEvent, MessageEvent, WebSocket};

use std::cell::RefCell;
use std::rc::Rc;

use super::*;
use crate::core::{socket, Logger};

#[derive(Clone, Copy)]
pub struct JsLogger;

impl Logger for JsLogger {
    fn info<M>(&self, message: M)
    where
        M: AsRef<str>,
    {
        consolelog::clog(message.as_ref());
    }
    fn error<M>(&self, message: M)
    where
        M: AsRef<str>,
    {
        consolelog::clog(&format!("error: {}", message.as_ref()));
    }
}

pub type Core<UI> = crate::core::Core<JsLogger, UI, web_sys::WebSocket>;

pub fn create_core<UI>(host: &str, ui: UI) -> Result<Rc<RefCell<Core<UI>>>, JsValue>
where
    UI: crate::core::UserInterface + 'static,
{
    let server = format!("ws://{}/socketserver", host);
    console_log!(r#"Creating websocket connected to server: "{}""#, server);
    let ws = WebSocket::new(&server).expect("Can not open websocket");
    console_log!("Creating websocket succeeded!");
    let core = Rc::new(RefCell::new(Core::new(ws, ui, JsLogger {})));
    core.borrow()
        .program_socket(|socket: &web_sys::WebSocket| set_core_input(socket, &core));
    Ok(core)
}

fn set_core_input<UI>(socket: &web_sys::WebSocket, core: &Rc<RefCell<Core<UI>>>)
where
    UI: crate::core::UserInterface + 'static,
{
    //TODO do something about forget

    use socket::InSocket;

    let onerror_callback = {
        let core = core.clone();
        Closure::wrap(
            Box::new(move |e: ErrorEvent| core.borrow_mut().in_socket().on_error(e))
                as Box<dyn FnMut(ErrorEvent)>,
        )
    };
    socket.set_onerror(Some(onerror_callback.as_ref().unchecked_ref()));
    onerror_callback.forget();

    let onopen_callback = {
        let core = core.clone();
        Closure::wrap(
            Box::new(move |_| core.borrow_mut().in_socket().on_open()) as Box<dyn FnMut(JsValue)>
        )
    };
    socket.set_onopen(Some(onopen_callback.as_ref().unchecked_ref()));
    onopen_callback.forget();

    let onclose_callback = {
        let core = core.clone();
        Closure::wrap(
            Box::new(move |_| core.borrow_mut().in_socket().on_closed()) as Box<dyn FnMut(JsValue)>
        )
    };
    socket.set_onclose(Some(onclose_callback.as_ref().unchecked_ref()));
    onclose_callback.forget();

    let on_message_callback = {
        let core = core.clone();
        Closure::wrap(Box::new(move |e: MessageEvent| {
            match e.data().as_string() {
                Some(msg) => core.borrow_mut().in_socket().on_message(msg),
                None => {
                    console_log!(
                        "Error: Received a message, but it was not a text message : {:?}",
                        e
                    );
                }
            };
        }) as Box<dyn FnMut(MessageEvent)>)
    };
    socket.set_onmessage(Some(on_message_callback.as_ref().unchecked_ref()));
    on_message_callback.forget();
}
