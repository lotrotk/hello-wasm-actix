use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{Document, ErrorEvent, HtmlElement, HtmlInputElement};

mod core;
mod web;

use std::rc::Rc;

type StateMachine = web::Core<HtmlInterface>;
type StateMachinePtr = Rc<std::cell::RefCell<StateMachine>>;

static PLAYER_NAME_ID: &str = "PLAYER_NAME";
static SESSION_ID: &str = "SESSION";

fn document_and_body() -> Option<(Document, HtmlElement)> {
    web_sys::window().and_then(|window| {
        if let Some(document) = window.document() {
            if let Some(body) = document.body() {
                return Some((document, body));
            }
        }
        None
    })
}

fn add_text(document: &Document, body: &HtmlElement) -> HtmlElement {
    let par = create_html_element(document, "p");
    par.set_inner_html("<div>Hello World!<div>");
    body.append_child(&par).ok();
    par
}

fn host(document: &Document) -> Option<String> {
    document
        .location()
        .and_then(|location| location.host().ok())
}

fn create_html_element(document: &Document, html_type: &str) -> HtmlElement {
    HtmlElement::from(JsValue::from(document.create_element(html_type).unwrap()))
}

fn create_html_input_element(document: &Document, html_type: &str) -> HtmlInputElement {
    HtmlInputElement::from(JsValue::from(document.create_element(html_type).unwrap()))
}

fn add_newline(document: &Document, body: &HtmlElement) {
    let newline = create_html_element(&document, "br");
    body.append_child(&newline).unwrap();
}

struct Button {
    button: HtmlElement,
}

impl Button {
    fn new(document: &Document, body: &HtmlElement, txt: &str) -> Self {
        let button = create_html_element(&document, "button");
        button.set_inner_text(txt);
        body.append_child(&button).unwrap();

        Self { button }
    }

    fn set_callback<F>(
        &self,
        document: &Document,
        element_name: &'static str,
        state_machine: &StateMachinePtr,
        functor: F,
    ) where
        F: FnOnce(&mut StateMachine, String) + Copy + 'static,
    {
        let onclick = {
            let (document, state_machine) = (document.clone(), state_machine.clone());
            Closure::wrap(Box::new(move |_| {
                let element = document.get_element_by_id(element_name).unwrap();
                let element = element.dyn_into::<HtmlInputElement>().unwrap();
                let value = element.value();
                functor(&mut state_machine.borrow_mut(), value);
            }) as Box<dyn FnMut(ErrorEvent)>)
        };
        self.button
            .set_onclick(Some(onclick.as_ref().unchecked_ref()));
        onclick.forget();
    }
}

struct Name {
    name_value_label: HtmlElement,
    name_field: HtmlInputElement,
}

impl Name {
    fn new(
        document: &Document,
        body: &HtmlElement,
        description_txt: &str,
        button_txt: &str,
        value_id: &str,
    ) -> (Self, Button) {
        let name_label = create_html_element(&document, "label");
        name_label.set_inner_text(description_txt);
        body.append_child(&name_label).unwrap();
        let name_value_label = create_html_element(&document, "label");
        body.append_child(&name_value_label).unwrap();

        let name_field = create_html_input_element(&document, "input");
        name_field.set_id(value_id);
        body.append_child(&name_field).unwrap();

        let rename_button = Button::new(document, body, button_txt);

        let this = Self {
            name_value_label,
            name_field,
        };
        (this, rename_button)
    }
}

fn create_active_session(document: &Document, body: &HtmlElement) -> (Name, Button, Button) {
    let (name, participate_button) =
        Name::new(document, body, "Session: ", "Participate", SESSION_ID);
    let create_session_button = Button::new(document, body, "Create Session");
    (name, participate_button, create_session_button)
}

struct Sessions {
    label: HtmlElement,
}

impl Sessions {
    fn new(document: &Document, body: &HtmlElement) -> Self {
        let label = create_html_element(&document, "label");
        body.append_child(&label).unwrap();
        let mut s = Self { label };
        s.set_titles(std::iter::empty());
        s
    }

    fn set_titles<I: Iterator<Item = core::SessionDescription>>(&mut self, it: I) {
        let txt = it.fold("Active Sessions:\n".to_string(), |mut txt, description| {
            let core::SessionDescription {
                id: _id,
                title,
                game_type,
                participating,
            } = description;
            let p = match participating {
                true => "<== Participating",
                false => "",
            };
            txt += &format!("{} ({}){}\n", title, game_type, p);
            txt
        });
        self.label.set_inner_text(&txt);
    }
}

struct HtmlInterface {
    server_label: HtmlElement,
    name: Name,
    current_session: Name,
    sessions: Sessions,
}

impl core::UserInterface for HtmlInterface {
    fn alert(&self, msg: &str) {
        self::alert(msg)
    }

    fn set_sessions<I: Iterator<Item = core::SessionDescription>>(&mut self, it: I) {
        self.sessions.set_titles(it)
    }

    fn set_server_name(&mut self, name: String) {
        self.server_label
            .set_inner_html(&format!("<div>Hello {}!<div>", name));
    }

    fn rename(&mut self, new_name: &str) {
        self.name.name_field.set_value("");
        self.name.name_value_label.set_inner_text(new_name);
    }

    fn new_game_of_mines_participating(
        &mut self,
        title: String,
        session: core::Session,
        width: u32,
        height: u32,
        mines: u32,
    ) {
        self.current_session.name_field.set_value("");
        self.current_session.name_value_label.set_inner_text(&title);
    }
}

fn my_main() {
    let (document, body) = document_and_body().expect("should have a document and body");
    let document = Rc::new(document);
    let server_label = add_text(&document, &body);
    let host = host(&document).expect("should have a host");

    let (name, name_rename_button) =
        Name::new(&document, &body, "Player Name: ", "Rename", PLAYER_NAME_ID);
    add_newline(&document, &body);
    let (current_session, participate_button, session_create_button) =
        create_active_session(&document, &body);
    add_newline(&document, &body);
    let sessions = Sessions::new(&document, &body);
    add_newline(&document, &body);

    let html_interface = HtmlInterface {
        name,
        server_label,
        sessions,
        current_session,
    };
    let state_machine = web::create_core(&host, html_interface).expect("failed to create core");

    name_rename_button.set_callback(&document, PLAYER_NAME_ID, &state_machine, |sm, name| {
        sm.register_player(name)
    });
    session_create_button.set_callback(&document, SESSION_ID, &state_machine, |sm, name| {
        sm.create_game_of_mines(name)
    });
    participate_button.set_callback(&document, SESSION_ID, &state_machine, |sm, name| {
        sm.participate_game(name)
    });
}

#[wasm_bindgen(start)]
pub fn main() {
    my_main()
}

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}
